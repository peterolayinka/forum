<?php
//This page let delete a category
include('config.php');
include('admin-right.php');
if(isset($_GET['id'])){
	$id = intval($_GET['id']);
	$dn1 = mysql_fetch_array(mysql_query('select count(id) as nb1, name, position, id from categories where id="'.$id.'" group by id'));
	if($dn1['nb1']>0){
		if(isset($_SESSION['username']) and ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1)) {
?>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Delete a category - <?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?> - Forum</title>
    </head>
    <body>
    	<?php include_once ('header.php'); ?>
    	<div class="container">
    		<?php include ('ads.php'); ?>
    	</div>

        <div class="container content">
			<?php
			$nb_new_pm = mysql_fetch_array(mysql_query('select count(*) as nb_new_pm from pm where ((user1="'.$_SESSION['userid'].'" and user1read="no") or (user2="'.$_SESSION['userid'].'" and user2read="no")) and id2="1"'));
			$nb_new_pm = $nb_new_pm['nb_new_pm'];

			?>
<div class="page-title page-breadcrumb">
	<ul>
		<li>
			<a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
		</li>
		<li>
			<a href="list_topics.php?parent=<?php echo $dn1['id']; ?>" class="title">
			<?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?>
			</a>
		</li>
		<li>
			Delete category
		</li>
	</ul>
</div>
<div class="main-content">
<?php
if(isset($_POST['confirm']))
{
	if(mysql_query('delete from categories where id="'.$id.'"') and mysql_query('delete from topics where parent="'.$id.'"') and mysql_query('update categories set position=position-1 where position>"'.$dn1['position'].'"'))
	{
	?>
	<div class="message">The category and it topics have successfully been deleted.</div>
	<?php
		header("refresh:3; url=index.php");
	}
	else
	{
		echo 'An error occured while deleting the category and it topics.';
	}
}
else
{
?>
	<form action="delete_category.php?id=<?php echo $id; ?>" method="post">
		Are you sure you want to delete this category and all it topics?
	    <input type="hidden" name="confirm" value="true" />
	    <input type="submit" value="Yes" /> <input type="button" value="No" onclick="javascript:history.go(-1);" />
	</form>
<?php
}
?>
	</div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>
<?php
}
else
{
	echo '<h2>You must be logged as an administrator to access this page: <a href="login.php">Login</a> - <a href="signup.php">Sign Up</a></h2>';
}
}
else
{
	echo '<h2>The category you want to delete doesn\'t exist.</h2>';
}
}
else
{
	echo '<h2>The ID of the category you want to delete is not defined.</h2>';
}
?>