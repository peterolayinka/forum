<?php
//This page let display the list of personnal message of an user
include('config.php');
include('user-right.php');

$r_page = (int) (!isset($_GET["rpage"]) ? 1 : $_GET["rpage"]);
$r_startpoint = ($r_page * $limit) - $limit;
$r_reload = "?";
$r_statement = 'pm as m1, pm as m2,users where ((m1.user1="'.$_SESSION['userid'].'" and m1.user1read="yes" and users.id=m1.user2) or (m1.user2="'.$_SESSION['userid'].'" and m1.user2read="yes" and users.id=m1.user1)) and m1.id2="1" and m2.id=m1.id';

$u_page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$u_startpoint = ($u_page * $limit) - $limit;
$u_reload = "?";
$u_statement = 'pm as m1, pm as m2,users where ((m1.user1="'.$_SESSION['userid'].'" and m1.user1read="no" and users.id=m1.user2) or (m1.user2="'.$_SESSION['userid'].'" and m1.user2read="no" and users.id=m1.user1)) and m1.id2="1" and m2.id=m1.id';

?>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Personal Messages</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
        <div class="container">
            <?php include ('ads.php'); ?>
        </div>
        <div class="container content">
<?php
if(isset($_SESSION['username'])){

    $u_total =mysql_query('select count(m2.id) as reps, users.id as userid, users.username from pm as m1, pm as m2,users where ((m1.user1="'.$_SESSION['userid'].'" and m1.user1read="no" and users.id=m1.user2) or (m1.user2="'.$_SESSION['userid'].'" and m1.user2read="no" and users.id=m1.user1)) and m1.id2="1" and m2.id=m1.id group by m1.id');
    $unread_c = intval(mysql_num_rows($u_total));
    $unread_s = 1;

    $r_total =mysql_query('select count(m2.id) as reps, users.id as userid, users.username from pm as m1, pm as m2,users where ((m1.user1="'.$_SESSION['userid'].'" and m1.user1read="yes" and users.id=m1.user2) or (m1.user2="'.$_SESSION['userid'].'" and m1.user2read="yes" and users.id=m1.user1)) and m1.id2="1" and m2.id=m1.id group by m1.id');
    $read_c = intval(mysql_num_rows($r_total));
    $read_s = 1;

$req1 = mysql_query('select m1.id, m1.title, m1.timestamp, count(m2.id) as reps, users.id as userid, users.username from '. $u_statement .' group by m1.id order by m1.id desc limit '. $u_startpoint . ', '. $limit);
$req2 = mysql_query('select m1.id, m1.title, m1.timestamp, count(m2.id) as reps, users.id as userid, users.username from '. $r_statement .' group by m1.id order by m1.id desc limit '. $r_startpoint . ', '. $limit );

$nb_new_pm = mysql_fetch_array(mysql_query('select count(*) as nb_new_pm from pm where ((user1="'.$_SESSION['userid'].'" and user1read="no") or (user2="'.$_SESSION['userid'].'" and user2read="no")) and id2="1"'));
$nb_new_pm = $nb_new_pm['nb_new_pm'];

$nb_all_pm = mysql_fetch_array(mysql_query('select count(*) as nb_all_pm from pm where ((user1="'.$_SESSION['userid'].'") or (user2="'.$_SESSION['userid'].'")) and id2="1"'));
$nb_all_pm = $nb_all_pm['nb_all_pm'];
?>
<div class="page-title page-breadcrumb">
    <ul>
        <li>
            <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
        </li>
        <li>
            Messages
        </li>
        <li>
            <a href="new_pm.php" >New Personal Message</a>
        </li>
    </ul>
</div>
<div class="main-content">
<h3>Unread messages(<?php echo $unread_c; ?>): all(<?php echo $nb_all_pm ?>)</h3><hr>
<table class="list_pm">
<?php
while($dn1 = mysql_fetch_array($req1))
{
?>
    <tr >
        <td class="message-item message-item-unread" ><a href="read_pm.php?id=<?php echo $dn1['id']; ?>"><?php echo htmlentities($dn1['title'], ENT_QUOTES, 'UTF-8'); ?></a> from <a href="profile.php?id=<?php echo $dn1['userid']; ?>"><?php echo htmlentities($dn1['username'], ENT_QUOTES, 'UTF-8'); ?></a> <?php echo check_time($dn1['timestamp']); ?> (<?php echo $dn1['reps']-1; ?> replies)
        </td>
    </tr>
<?php
}
if(intval(mysql_num_rows($req1))==0)
{
?>
	<tr>
    	<td class="center">You have no unread message.</td>
    </tr>
<?php
}
?>
</table>
<div class="content-footer" style="margin-bottom: 10px;">
    <ul>
        <li class="content-footer__item">
            <?php echo pagination($u_statement,$limit,$u_page,$u_reload,$unread_c, $unread_s);?>
        </li>
    </ul>
</div>
<h3>Read messages(<?php echo $read_c; ?>):</h3><hr>
<table>
<?php
while($dn2 = mysql_fetch_array($req2))
{
?>
	<tr >
    	<td class="message-item" ><a href="read_pm.php?id=<?php echo $dn2['id']; ?>"><?php echo htmlentities($dn2['title'], ENT_QUOTES, 'UTF-8'); ?></a> from <a href="profile.php?id=<?php echo $dn2['userid']; ?>"><?php echo htmlentities($dn2['username'], ENT_QUOTES, 'UTF-8'); ?></a> <?php echo check_time($dn2['timestamp']); ?> (<?php echo $dn2['reps']-1; ?> replies)
        </td>
    </tr>
<?php
}
if(intval(mysql_num_rows($req2))==0)
{
?>
	<tr>
    	<td class="center">You have no read message.</td>
    </tr>
<?php
}
?>
</table>
<div class="content-footer">
    <ul>
        <li class="content-footer__item">
            <?php echo r_pagination($r_statement,$limit,$r_page,$r_reload,$read_c, $read_s);?>
        </li>
    </ul>
</div>
<?php
}
else

{
?>
<h2>You must be logged to access this page:</h2>
<div class="box_login">
	<form action="login.php" method="post">
		<label for="username">Username</label><input type="text" name="username" id="username" /><br />
		<label for="password">Password</label><input type="password" name="password" id="password" /><br />
        <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" />
        <div class="center">
	        <input type="submit" value="Login" /> <input type="button" onclick="javascript:document.location='signup.php';" value="Sign Up" />
        </div>
    </form>
</div>
<?php
}
?>
    </div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>