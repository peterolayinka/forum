<?php
//This page let log in
include('config.php');
if(isset($_SESSION['username'])){
	unset($_SESSION['username'], $_SESSION['userid'], $_SESSION['perm']);
	setcookie('username', '', time()-100);
	setcookie('password', '', time()-100);
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Login</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
    	<div class="container">
            <?php include ('ads.php'); ?>
        </div>
    	<div class="container content">
    		<div class="main-content center">
				<div class="message">You have successfully been logged out.</div>
			</div>
		</div>
<?php
		header("refresh:3; url=index.php");
}else{

	$ousername = '';
	if(isset($_POST['username'], $_POST['password'])){
		if(get_magic_quotes_gpc()){
			$ousername = stripslashes($_POST['username']);
			$username = mysql_real_escape_string(stripslashes($_POST['username']));
			$password = stripslashes($_POST['password']);
		}else{
			$username = mysql_real_escape_string($_POST['username']);
			$password = $_POST['password'];
		}
		$req = mysql_query('select password, id, ban, perm_level from users where username="'.$username.'"');
		$dn = mysql_fetch_array($req);
		if($dn['password']==sha1($password) and mysql_num_rows($req)>0){
			if ($dn['ban'] == 1) {
				$form = true;
				$message = 'You are being ban, contact the admin.';
			}else{
				$form = false;
				$_SESSION['username'] = $_POST['username'];
				$_SESSION['perm'] = $dn['perm_level'];
				$_SESSION['userid'] = $dn['id'];
				if(isset($_POST['memorize']) and $_POST['memorize']=='yes'){
					$one_year = time()+(60*60*24*365);
					setcookie('username', $_POST['username'], $one_year);
					setcookie('password', sha1($password), $one_year);
				}
				$message = 'You have successfully been logged.';
			}?>

			<?php
		}else{
			$form = true;
			$message = 'The username or password you entered are not good.';
		}
	}else{
		$form = true;
	}
	if($form){ ?>
		<!DOCTYPE html >
		<html>
		    <head>
		        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		        <meta name="viewport" content="width=device-width, initial-scale=1.0">
		        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
		        <title>Login</title>
		    </head>
		    <body>
		    	<?php include_once ('header.php');?>
		    	<div class="container">
		            <?php include ('ads.php'); ?>
		        </div>
		<?php
		if(isset($message)){ ?>
			<div class="container content">
		    	<div class="main-content center">
					<div class="message"><?php echo $message; ?></div>
				</div>
			</div>

		<?php
		}?>
		<div class="container content">
			<?php
			$nb_new_pm = mysql_fetch_array(mysql_query('select count(*) as nb_new_pm from pm where ((user1="'.$_SESSION['userid'].'" and user1read="no") or (user2="'.$_SESSION['userid'].'" and user2read="no")) and id2="1"'));
			$nb_new_pm = $nb_new_pm['nb_new_pm'];
			?>
			<div class="page-title page-breadcrumb">
				<ul>
					<li>
						<a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
					</li>
					<li>
						Login
					</li>
				</ul>
			</div>
			<div class="main-content center">
			    <form action="login.php" method="post">
			        Please, type your IDs to log:<br />
			        <div class="login">
			            <label for="username">Username</label><input type="text" name="username" id="username" value="<?php echo htmlentities($ousername, ENT_QUOTES, 'UTF-8'); ?>" /><br />
			            <label for="password">Password</label><input type="password" name="password" id="password" /><br />
			            <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" /><br />
			            <input type="submit" value="Login" />
					</div>
			    </form>
			</div>
		</div>
<?php
	}else{ ?>
		<!DOCTYPE html>
			<html >
			    <head>
			        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			        <meta name="viewport" content="width=device-width, initial-scale=1.0">
			        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
			        <title>Login</title>
			    </head>
			    <body>
			    	<?php include_once ('header.php');?>
			    	<div class="container">
			            <?php include ('ads.php'); ?>
			        </div>
			    	<div class="container content">
				    	<div class="main-content center">
							<div class="message"><?php echo $message;
								header("refresh:3; url=index.php");
							?></div>
						</div>
					</div>
	<?php
	}
}
?>
		<?php include_once ('footer.php'); ?>
	</body>
</html>