<?php
//This page let display the list of topics of a category
include('config.php');
include('user-right.php');
if(isset($_GET['parent'])){
	$id = intval($_GET['parent']);
	$dn1 = mysql_fetch_array(mysql_query('select count(c.id) as nb1, c.name,count(t.id) as topics from categories as c left join topics as t on t.parent="'.$id.'" where c.id="'.$id.'" group by c.id'));
if($dn1['nb1']>0)
{
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title><?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?> - Forum</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
        <div class="container">
            <?php include ('ads.php'); ?>
        </div>
        <div class="container content">
            <div class="page-title page-breadcrumb">
                <ul>
                    <li>
                        <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
                    </li>
                    <li>
                        <?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?>
                    </li>
                    <li>
                        <?php if(isset($_SESSION['username'])){
                        ?>
                            <a href="new_topic.php?parent=<?php echo $id; ?>">New Topic</a>
                        <?php
                        } ?>
                    </li>
                </ul>
            </div>
            <div class="main-content">
            <?php
            $dn2 = mysql_query('select t.id, t.title, t.authorid, u.username as author, count(r.id) as replies from topics as t left join topics as r on r.parent="'.$id.'" and r.id=t.id and r.id2!=1  left join users as u on u.id=t.authorid where t.parent="'.$id.'" and t.id2=1 group by t.id order by t.timestamp2 desc');
            if(mysql_num_rows($dn2)>0){ ?>
                <table class="topics_table">
                	<tr>
                    	<th class="forum_tops">Topic</th>
                    	<th class="forum_auth">Author</th>
                    	<th class="forum_nrep">Replies</th>
                <?php
                if(isset($_SESSION['username']) and $_SESSION['username']==$admin){ ?>
                    	<th class="forum_act">Action</th>
                <?php
                } ?>
                	</tr>
                <?php
                while($dnn2 = mysql_fetch_array($dn2)){ ?>
                	<tr>
                    	<td class="forum_tops"><a href="read_topic.php?id=<?php echo $dnn2['id']; ?>"><?php echo htmlentities($dnn2['title'], ENT_QUOTES, 'UTF-8'); ?></a></td>
                    	<td><a href="profile.php?id=<?php echo $dnn2['authorid']; ?>"><?php echo htmlentities($dnn2['author'], ENT_QUOTES, 'UTF-8'); ?></a></td>
                    	<td><?php echo $dnn2['replies']; ?></td>
                        <?php
                        if(isset($_SESSION['username']) and $_SESSION['username']==$admin) { ?>
                            	<td><a href="delete_topic.php?id=<?php echo $dnn2['id']; ?>"><img src="<?php echo $design; ?>/images/delete.png" alt="Delete" /></a></td>
                        <?php
                        }?>
                    </tr>
                <?php
                }?>
                </table>
            <?php
            }else{ ?>
                <div class="message">This category has no topic.</div>
            <?php
            }
            if(!isset($_SESSION['username'])){ ?>
                <div class="box_login">
                	<form action="login.php" method="post">
                		<label for="username">Username</label><input type="text" name="username" id="username" /><br />
                		<label for="password">Password</label><input type="password" name="password" id="password" /><br />
                        <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" />
                        <div class="center">
                	        <input type="submit" value="Login" /> <input type="button" onclick="javascript:document.location='signup.php';" value="Sign Up" />
                        </div>
                    </form>
                </div>
            <?php
            }?>
                    <div class="content-footer">
                        <ul>
                            <li class="content-footer__item">
                                <ul class="pagination">
                                    <li>(1)</li>
                                    <li>(2)</li>
                                    <li>(3)</li>
                                    <li>(4)</li>
                                    <li>(5)</li>
                                    <li>(6)</li>
                                    <li>(7)</li>
                                    <li>(8)</li>
                                    <li>(9)</li>
                                </ul>
                            </li>
                            <?php
                            if(isset($_SESSION['username'])){ ?>
                            <li class="content-footer__item">
                                <a href="new_topic.php?parent=<?php echo $id; ?>">New Topic</a>
                            </li>
                            <?php
                            } ?>
                        </ul>
                    </div>
                </div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>
<?php
}
else
{
	echo '<h2>This category doesn\'t exist.</h2>';
}
}
else
{
	echo '<h2>The ID of the category you want to visit is not defined.</h2>';
}
?>