<?php
//This page let an user edit a message
include('config.php');
include('user-right.php');
if(isset($_GET['id'], $_GET['id2']))
{
	$id = intval($_GET['id']);
	$id2 = intval($_GET['id2']);
if(isset($_SESSION['username']))
{
	$dn1 = mysql_fetch_array(mysql_query('select count(t.id) as nb1, t.authorid, t2.title, t.message, t.parent, c.name from topics as t, topics as t2, categories as c where t.id="'.$id.'" and t.id2="'.$id2.'" and t2.id="'.$id.'" and t2.id2=1 and c.id=t.parent group by t.id'));
	$u_upload = mysql_fetch_array(mysql_query('select count(*) as up1, real_name, new_name from uploads as u where u.id1="'.$id.'" and u.id2="'.$id2.'" and u.parent="'.$dn1['parent'].'"'));

if($dn1['nb1']>0)
{
if($_SESSION['userid']==$dn1['authorid'] or ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1))
{
include('bbcode_function.php');
?>
<!DOCTYPE>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Edit a reply - <?php echo htmlentities($dn1['title'], ENT_QUOTES, 'UTF-8'); ?> - <?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?> - Forum</title>
		<script type="text/javascript" src="functions.js"></script>
    </head>
    <body>
    	<?php include_once ('header.php');?>
    	<div class="container">
    		<?php include ('ads.php'); ?>
    	</div>
        <div class="container content">

<div class="page-title page-breadcrumb">
	<ul>
		<li>
			<a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
		</li>
		<li>
			<a href="list_topics.php?parent=<?php echo $dn1['parent']; ?>"><?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?></a>
		</li>
		<li>
			<a href="read_topic.php?id=<?php echo $id; ?>"><?php echo htmlentities($dn1['title'], ENT_QUOTES, 'UTF-8'); ?></a>
		</li>
		<li>
			Edit reply
		</li>
	</ul>
</div>
<div class="main-content">
<?php
if(isset($_POST['message']) and $_POST['message']!='')
{
	if($id2==1)
	{
		if(($_SESSION['username']==$admin || $_SESSION['perm'] >= 1) and isset($_POST['title']) and $_POST['title']!='')
		{
			$title = $_POST['title'];
			if(get_magic_quotes_gpc())
			{
				$title = stripslashes($title);
			}
			$title = mysql_real_escape_string($dn1['title']);
		}
		else
		{
			$title = mysql_real_escape_string($dn1['title']);
		}
	}
	else
	{
		$title = '';
	}
	$message = $_POST['message'];
	if(get_magic_quotes_gpc())
	{
		$message = stripslashes($message);
	}
	$message = mysql_real_escape_string(bbcode_to_html($message));
	if(mysql_query('update topics set title="'.$title.'", message="'.$message.'" where id="'.$id.'" and id2="'.$id2.'"'))
	{


		if(isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])) {
      $status= array();
      $file_name = $_FILES['image']['name'];
      $file_size = $_FILES['image']['size'];
      $file_tmp = $_FILES['image']['tmp_name'];
      $file_type = $_FILES['image']['type'];
      $file_string = explode('.',$file_name);
      $file_ext = strtolower(end($file_string));
      $file_new_name = md5(uniqid(rand(), true)) .'.'. $file_ext;

      $expensions= array("jpeg","jpg","png","doc","docx","xlsx","ppt", "pptx", "pdf");

      if(in_array($file_ext,$expensions)=== false){
        $status="Extension not allowed, please choose a JPEG or PNG file.";

	      	if($file_size > 2097152) {
	         $status='File size must be excately 2 MB';
	      }
      }

      if(empty($status)==true) {
      	$u_upload = mysql_fetch_array(mysql_query('select count(*) as up1, real_name, new_name from uploads as u where u.id1="'.$id.'" and u.id2="'.$id2.'" and u.parent="'.$dn1['parent'].'"'));

      	// delete earlier image
      	unlink('uploads/'.$u_upload['new_name']);

      	move_uploaded_file($file_tmp,"uploads/".$file_new_name);
      	$q_upload = mysql_query("UPDATE uploads SET real_name='".$file_name."', new_name='".$file_new_name."' WHERE id1='".$id."' and id2='".$id2."' and parent='". $dn1['parent']."'");
      	$status="Success";
      }
   }
	?>
	<div class="message">The message have successfully been sent.
	<?php
		if (isset($status)) {
			echo "". $status;
		}

		header("refresh:3; url=read_topic.php?id=". $id);
	?>
	</div>


	?>
	<?php
	}
	else
	{
		echo 'An error occurred while editing the message.';
	}
}
else
{
?>
<form action="edit_message.php?id=<?php echo $id; ?>&id2=<?php echo $id2; ?>" method="post" enctype="multipart/form-data">
<?php
if(($_SESSION['username']==$admin || $_SESSION['perm'] >= 1) and $id2==1)
{
?>
	<label for="title">Title</label><input type="text" name="title" id="title" value="<?php echo htmlentities($dn1['title'], ENT_QUOTES, 'UTF-8'); ?>" />
<?php
}
?>
    <label for="message">Message</label><br />
    <div class="message_buttons">
        <input type="button" value="Bold" onclick="javascript:insert('[b]', '[/b]', 'message');" /><!--
        --><input type="button" value="Italic" onclick="javascript:insert('[i]', '[/i]', 'message');" /><!--
        --><input type="button" value="Underlined" onclick="javascript:insert('[u]', '[/u]', 'message');" /><!--
        --><input type="button" value="Image" onclick="javascript:insert('[img]', '[/img]', 'message');" /><!--
        --><input type="button" value="Link" onclick="javascript:insert('[url]', '[/url]', 'message');" /><!--
        --><input type="button" value="Left" onclick="javascript:insert('[left]', '[/left]', 'message');" /><!--
        --><input type="button" value="Center" onclick="javascript:insert('[center]', '[/center]', 'message');" /><!--
        --><input type="button" value="Right" onclick="javascript:insert('[right]', '[/right]', 'message');" /><!--
        --><input type="button" value="Youtube" onclick="javascript:insert('[youtube]', '[/youtube]', 'message');" />
    </div>
    <textarea name="message" id="message" cols="70" rows="6"><?php echo html_to_bbcode($dn1['message']); ?></textarea><br />
    <label> Attachment (jpeg, jpg, png, doc, docx, xlsx, ppt, pptx, pdf)
    <input type="file" name = "image" /></label><br /><br />
    <input type="submit" value="Send" />
</form>
<?php
}
?>
	</div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>
<?php
}
else
{
	echo '<h2>You don\'t have the right to edit this message.</h2>';
}
}
else
{
	echo '<h2>The message you want to edit doesn\'t exist..</h2>';
}
}
else
{
?>
<h2>You must be logged to access this page:</h2>
<div class="box_login">
	<form action="login.php" method="post">
		<label for="username">Username</label><input type="text" name="username" id="username" /><br />
		<label for="password">Password</label><input type="password" name="password" id="password" /><br />
        <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" />
        <div class="center">
	        <input type="submit" value="Login" /> <input type="button" onclick="javascript:document.location='signup.php';" value="Sign Up" />
        </div>
    </form>
</div>
<?php
}
}
else
{
	echo '<h2>The ID of the message you want to edit is not defined.</h2>';
}
?>