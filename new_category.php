<?php
//This page let create a new category
include('config.php');
if(isset($_SESSION['username']) and ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1)){
?>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>New Category - Forum</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
    	<div class="container">
    		<?php include ('ads.php'); ?>
    	</div>
        <div class="container content">
<?php
$nb_new_pm = mysql_fetch_array(mysql_query('select count(*) as nb_new_pm from pm where ((user1="'.$_SESSION['userid'].'" and user1read="no") or (user2="'.$_SESSION['userid'].'" and user2read="no")) and id2="1"'));
$nb_new_pm = $nb_new_pm['nb_new_pm'];
?>
<div class="page-title page-breadcrumb">
	<ul>
		<li>
			<a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
		</li>
		<li>
			New Category
		</li>
	</ul>
</div>
<div class="main-content">
<?php
if(isset($_POST['name'], $_POST['description']) and $_POST['name']!='')
{
	$name = $_POST['name'];
	$description = $_POST['description'];
	$sub_id = $_POST['sub_id'];
	if(get_magic_quotes_gpc())
	{
		$name = stripslashes($name);
		$description = stripslashes($description);
		$sub_id = stripslashes($sub_id);
	}
	$name = mysql_real_escape_string($name);
	$description = mysql_real_escape_string($description);
	if(mysql_query('insert into categories (id, name, description, sub_id, position) select ifnull(max(id), 0)+1, "'.$name.'", "'.$description.'", '.$sub_id.', count(id)+1 from categories'))
	{
	?>
	<div class="message">The category have successfully been created.</div>
	<?php
		header("refresh:3; url=index.php");
	}
	else
	{
		echo 'An error occured while creating the category.';
	}
}
else
{
?>
<form action="new_category.php" method="post">
	<label for="name">Name</label><input type="text" name="name" id="name" /><br />
	<label for="sub_id" >Sub Category</label>
	<select name="sub_id" id="sub_id">
		<option>1</option>
		<option>2</option>
		<option>3</option>
	</select><br />
	<label for="description">Description</label>(html enabled)<br />
    <textarea name="description" id="description" cols="70" rows="6"></textarea><br />
    <input type="submit" value="Create" />
</form>
<?php
}
?>
	</div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>
<?php
}
else
{
	echo '<h2>You must be logged as an administrator to access this page: <a href="login.php">Login</a> - <a href="signup.php">Sign Up</a></h2>';
}
?>