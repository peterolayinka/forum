<?php
//This page display the profile of an user
include('config.php');
include('admin-right.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Profile of an user</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
        <div class="container">
            <?php include ('ads.php'); ?>
        </div>
        <div class="container content">

	        <div class="page-title page-breadcrumb">
	            <ul>
	                <li>
	                    <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
	                </li>
	                <li>
	                    Profile
	                </li>
	            </ul>
	        </div>
	        <div class="main-content">
						<table style="text-align: left;">
							<tr>
								<td>
									<?php
									if(isset($_POST['submit'])) {
										$ads_1_link = $_POST['ads_1_link'];
										$ads_1_image = $_POST['ads_1_image'];
										$ads_2_link = $_POST['ads_2_link'];
										$ads_2_image = $_POST['ads_2_image'];
										$ads_3_link = $_POST['ads_3_link'];
										$ads_3_image = $_POST['ads_3_image'];
										if(get_magic_quotes_gpc())
										{
											$ads_1_link = stripslashes($ads_1_link);
											$ads_1_image = stripslashes($ads_1_image);
											$ads_2_link = stripslashes($ads_2_link);
											$ads_2_image = stripslashes($ads_2_image);
											$ads_3_link = stripslashes($ads_3_link);
											$ads_3_image = stripslashes($ads_3_image);
										}
										$ads_1_link = mysql_real_escape_string($ads_1_link);
										$ads_1_image = mysql_real_escape_string($ads_1_image);
										$ads_2_link = mysql_real_escape_string($ads_2_link);
										$ads_2_image = mysql_real_escape_string($ads_2_image);
										$ads_3_link = mysql_real_escape_string($ads_3_link);
										$ads_3_image = mysql_real_escape_string($ads_3_image);

										$setting_query = mysql_query("UPDATE settings set ads1_link= '". $ads_1_link ."', ads1_image_url= '". $ads_1_image ."', ads2_link= '". $ads_2_link ."', ads2_image_url= '". $ads_2_image ."', ads3_link= '". $ads_3_link ."', ads3_image_url= '". $ads_3_image ."' WHERE id=1");
										if($setting_query){ ?>
											<div class="message">Setttngs has been Updated...</div>
										<?php
										}else{ ?>
											<div class="message">Settings Update failed, try again later...</div>

										<?php
										}
										?>

										<?php
										header("refresh:3; index.php");
									}else{
										$ads_query = "SELECT * FROM settings WHERE id=1";
										$ads_query = mysql_query($ads_query);
										$ads_data = mysql_fetch_array($ads_query);
										?>
										<form method="POST" action="">
											<label>Ads 1 Link
												<input type="text" name="ads_1_link" value="<?php echo $ads_data['ads1_link']; ?>">
											</label><br>
											<label>Ads 1 Image
												<input type="text" name="ads_1_image" value="<?php echo $ads_data['ads1_image_url']; ?>">
											</label><br>
											<label>Ads 2 Link
												<input type="text" name="ads_2_link" value="<?php echo $ads_data['ads2_link']; ?>">
											</label><br>
											<label>Ads 2 Image
												<input type="text" name="ads_2_image" value="<?php echo $ads_data['ads2_image_url']; ?>">
											</label><br>
											<label>Ads 3 Link
												<input type="text" name="ads_3_link" value="<?php echo $ads_data['ads3_link']; ?>">
											</label><br>
											<label>Ads 3 Image
												<input type="text" name="ads_3_image" value="<?php echo $ads_data['ads3_image_url']; ?>">
											</label><br><br>
											<input type="submit" name="submit" value="Submit" />
										</form>
										<?php
									}
									?>
								</td>
							</tr>
						</table>
					</div>
				</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>