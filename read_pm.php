<?php
//This page display a personnal message
include('config.php');
include('user-right.php');
?>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Read a PM</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
    	<div class="container">
            <?php include ('ads.php'); ?>
        </div>
<?php
if(isset($_SESSION['username'])){
	if(isset($_GET['id'])){
		$id = intval($_GET['id']);

		$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
		$startpoint = ($page * $limit) - $limit;
		$reload = "?id=". $id."&";
		$statement = "pm, users where pm.id=".$id." and users.id=pm.user1 order by pm.id2";

		$req1 = mysql_query("SELECT title, user1, user2 from pm where id=". $id ." and id2='1'");
		$dn1 = mysql_fetch_array($req1);
		if(mysql_num_rows($req1)==1){
			if($dn1['user1']==$_SESSION['userid'] or $dn1['user2']==$_SESSION['userid']){
				if($dn1['user1']==$_SESSION['userid']){
					mysql_query('update pm set user1read="yes" where id="'.$id.'" and id2="1"');
					$user_partic = 2;
				}else{
					mysql_query('update pm set user2read="yes" where id="'.$id.'" and id2="1"');
					$user_partic = 1;
				}
				$req2 = mysql_query("SELECT pm.timestamp, pm.id2, pm.message, users.id as userid, users.username, users.avatar from {$statement} LIMIT {$startpoint}, {$limit}");
				if(isset($_POST['message']) and $_POST['message']!=''){
					$message = $_POST['message'];
					if(get_magic_quotes_gpc())	{
						$message = stripslashes($message);
					}
					$message = mysql_real_escape_string(nl2br(htmlentities($message, ENT_QUOTES, 'UTF-8')));
					$message_sent = mysql_query('insert into pm (id, id2, title, user1, user2, message, timestamp, user1read, user2read)values("'.$id.'", "'.(intval(mysql_num_rows($req2))+1).'", "", "'.$_SESSION['userid'].'", "", "'.$message.'", "'.time().'", "", "")') and mysql_query('update pm set user'.$user_partic.'read="no" where id="'.$id.'" and id2="1"');
					if($message_sent){


						if(isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])) {
				      $status= array();
				      $file_name = $_FILES['image']['name'];
				      $file_size = $_FILES['image']['size'];
				      $file_tmp = $_FILES['image']['tmp_name'];
				      $file_type = $_FILES['image']['type'];
				      $file_string = explode('.',$file_name);
				      $file_ext = strtolower(end($file_string));
				      $file_new_name = md5(uniqid(rand(), true)) .'.'. $file_ext;

				      $expensions= array("jpeg","jpg","png","doc","docx","xlsx","ppt", "pptx", "pdf");

				      if(in_array($file_ext,$expensions)=== false){
				        $status="Extension not allowed, please choose a JPEG or PNG file.";

					      	if($file_size > 2097152) {
					         $status='File size must be excately 2 MB';
					      }
				      }

				      if(empty($status)==true) {
				      	move_uploaded_file($file_tmp,"uploads/".$file_new_name);
				      	$pm_upload = mysql_query('insert into pm_uploads (id1, id2, real_name, new_name)values("'.$id.'", "'.(intval(mysql_num_rows($req2))+1).'", "'.$file_name.'", "'.$file_new_name.'")');
				      	// die(mysql_error());
				      	$status="Success";
				      }
				   }
					?>
						<div class="container content ">
							<div class="main-content">
								<div class="message">
									Your reply has successfully been sent.
									<?php

									if (isset($status)) {
										echo "". $status;
									}
									header("refresh:3; read_pm.php?id=" . $id); ?>
								</div>
							</div>
						</div>
					<?php
					}else{  ?>
						<div class="container content">
							<div class="main-content">
								<div class="message">
									An error occurred while sending the reply.
									<?php

									if (isset($status)) {
										echo "". $status;
									}
									header("refresh:3; read_pm.php?id=" . $id); ?>
								</div>
							</div>
						</div>
					<?php
					}
				}else{
					?>
				<div class="container content">
					<div class="page-title page-breadcrumb">
		                <ul>
		                    <li>
		                        <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
		                    </li>
		                    <li>
		                        <a href="list_pm.php">Messages</a>
		                    </li>
		                    <li>
		                        View PM
		                    </li>
		                </ul>
		            </div>
				<div class="main-content">

					<h1><?php echo $dn1['title']; ?></h1>
					<table class="messages_table">
						<?php
						while($dn2 = mysql_fetch_array($req2)){	?>
							<tr>
								<td class="message-title">
									<?php
									if($_SESSION['username'] == $dn2['username']){ ?>
										<div class="date">
								    		<?php
											if($dn2['avatar']!=''){
												echo '<img src="'.htmlentities($dn2['avatar']).'" alt="Image Perso" style="max-width:100px;max-height:100px;" />';
											} ?>
								    		<a href="profile.php?id=<?php echo $dn2['userid']; ?>"><?php echo $dn2['username']; ?></a> @<?php echo date('Y/m/d H:i:s' ,$dn2['timestamp']); ?>
								    	</div>
									<?php
									} else { ?>
										<div class="date left">
								    		<?php
											if($dn2['avatar']!=''){
												echo '<img src="'.htmlentities($dn2['avatar']).'" alt="Image Perso" style="max-width:100px;max-height:100px;" />';
											}?>
								    		<a href="profile.php?id=<?php echo $dn2['userid']; ?>"><?php echo $dn2['username']; ?></a> @<?php echo date('Y/m/d H:i:s' ,$dn2['timestamp']); ?>
								    	</div>
									<?php } ?>
						    	</td>
						    </tr>
							<tr>
								<td>
								<?php
								if($_SESSION['username'] == $dn2['username']){ ?>

								<div class="message-box right">
							    	<?php echo $dn2['message']; ?>
					    	</div>
						    	<?php
								}else{?>
								<div class="message-box">
							    	<?php echo $dn2['message']; ?>
						    	</div>
								<?php
								} ?>
									<div>
										<?php
	                    $u_upload = mysql_fetch_array(mysql_query('select count(*) as up1, real_name, new_name from pm_uploads as u where u.id1="'.$id.'" and u.id2="'.$dn2['id2'].'"'));
	                        if($u_upload['up1']>0){
	                            $file_string = explode('.',$u_upload['real_name']);
	                            $file_ext = strtolower(end($file_string));

	                            $expensions= array("jpeg","jpg","png");

	                            if(in_array($file_ext,$expensions)=== false){
		                            if($_SESSION['username'] == $dn2['username']){  ?>
	                                <p class="attachments right">
	                                    <a href="uploads/<?php echo $u_upload['new_name']; ?>" title="<?php echo $u_upload['real_name']; ?>" target="blank"><?php echo $u_upload['real_name']; ?></a>
	                                </p>
	                              <?php }else{ ?>
	                                <p class="attachments">
	                                    <a href="uploads/<?php echo $u_upload['new_name']; ?>" title="<?php echo $u_upload['real_name']; ?>" target="blank"><?php echo $u_upload['real_name']; ?></a>
	                                </p>
		                            <?php
		                            	}
	                            }else{
	                            	if($_SESSION['username'] == $dn2['username']){ ?>
			                            <p class="attachments right">
			                                <img alt="<?php echo $u_upload['real_name']; ?>" src="uploads/<?php echo $u_upload['new_name']; ?>" title="<?php echo $u_upload['real_name']; ?>">
			                            </p>
	                            	<?php }else{ ?>
			                            <p class="attachments">
			                                <img alt="<?php echo $u_upload['real_name']; ?>" src="uploads/<?php echo $u_upload['new_name']; ?>" title="<?php echo $u_upload['real_name']; ?>">
			                            </p>
	                        <?php
	                        			}
	                           }

	                        } ?>
									</div>
								</td>
						  </tr>
						<?php
						}?>
					</table><br />

					<h2>Reply</h2>
					<div class="center">
					    <form action="read_pm.php?id=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
					    	<label for="message" class="center">Message</label><br />
					        <textarea cols="40" rows="5" name="message" id="message"></textarea><br />
					        <label> Attachment (jpeg, jpg, png, doc, docx, xlsx, ppt, pptx, pdf)
							    <input type="file" name = "image" /></label><br /><br />
							    <input type="submit" value="Send" />
					    </form>
					</div>

					<div class="content-footer">
                        <ul>
                            <li class="content-footer__item">
                                <?php echo pagination($statement,$limit,$page,$reload);?>
                            </li>
                        </ul>
                    </div>
				</div>
			<?php
				}
			}else{
				echo '<div class="message">You don\'t have the right to access this page.</div>';
			}
		}else{
			echo '<div class="message">This message doesn\'t exist.</div>';
		}
	}else{
		echo '<div class="message">The ID of this message is not defined.</div>';
	}
}else{ ?>
	<div class="container content">
		<div class="page-title page-breadcrumb">
		    <ul>
		        <li>
		            <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
		        </li>
		        <li>
		            Login
		        </li>
		    </ul>
		</div>
		<div class="main-content">
			<div class="message">You must be logged to access this page.</div>
			<div class="box_login">
				<form action="login.php" method="post">
					<label for="username">Username</label><input type="text" name="username" id="username" /><br />
					<label for="password">Password</label><input type="password" name="password" id="password" /><br />
			        <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" />
			        <div class="center">
				        <input type="submit" value="Login" /> <input type="button" onclick="javascript:document.location='signup.php';" value="Sign Up" />
			        </div>
			    </form>
			</div>
		</div>
	</div>
<?php
} ?>
	</div>
<?php
	include_once ('footer.php');
?>

	</body>
</html>