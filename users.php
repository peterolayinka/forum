<?php
//This page displays a list of all registered members
include('config.php');
include('admin-right.php');

$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);
$startpoint = ($page * $limit) - $limit;
$reload = "?";
$statement = 'users';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>List of all the users</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
         <div class="container">
            <?php include ('ads.php'); ?>
        </div>
        <div class="container content">

            <div class="page-title page-breadcrumb">
                <ul>
                    <li>
                        <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
                    </li>
                    <li>
                        Members
                    </li>
                </ul>
            </div>
<div class="main-content">
This is the list of all the users:
<table>
<?php
$req = mysql_query('select id, username, email from '. $statement . ' limit '. $startpoint . ', '. $limit);
while($dnn = mysql_fetch_array($req))
{
?>
	<tr>
    	<td><?php echo $dnn['id']; ?>) <a href="profile.php?id=<?php echo $dnn['id']; ?>"><?php echo htmlentities($dnn['username'], ENT_QUOTES, 'UTF-8'); ?></a> <?php echo htmlentities($dnn['email'], ENT_QUOTES, 'UTF-8'); ?>
    </tr>
<?php
}
?>
</table>
    <div class="content-footer">
            <ul>
                <li class="content-footer__item">
                    <?php echo pagination($statement,$limit,$page,$reload);?>
                </li>
            </ul>
        </div>
    </div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>