<?php
//This page let display the list of topics of a category
include('config.php');
// include('user-right.php');
if(isset($_GET['parent'])){
	$id = intval($_GET['parent']);
	$dn1 = mysql_fetch_array(mysql_query('select count(c.id) as nb1, c.name,count(t.id) as topics from categories as c left join topics as t on t.parent="'.$id.'" where c.id="'.$id.'" group by c.id'));
    $page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);

    $startpoint = ($page * $limit) - $limit;
    $reload = "?parent=". $id."&";

    $statement = 'topics as t left join topics as r on r.parent="'.$id.'" and r.id=t.id and r.id2!=1  left join users as u on u.id=t.authorid where t.parent="'.$id.'" and t.id2=1';

if($dn1['nb1']>0){
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title><?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?> - Forum</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
        <div class="container">
            <?php include ('ads.php'); ?>
        </div>
        <div class="container content">
            <div class="page-title page-breadcrumb">
                <ul>
                    <li>
                        <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
                    </li>
                    <li>
                        <?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?>
                    </li>
                    <li>
                        <?php if(isset($_SESSION['username'])){
                        ?>
                            <a href="new_topic.php?parent=<?php echo $id; ?>">New Topic</a>
                        <?php
                        } ?>
                    </li>
                </ul>
            </div>
            <div class="main-content">
            <?php
            $dn2 = mysql_query("SELECT t.id, t.title, t.timestamp, t.authorid, u.username as author, count(r.id) as replies from {$statement} group by t.id order by t.timestamp2 desc LIMIT {$startpoint} , {$limit}");
            if(mysql_num_rows($dn2)>0){ ?>
                <table class="topics_table">
                <?php
                while($dnn2 = mysql_fetch_array($dn2)){ ?>
                	<tr>
                    	<td class="forum_tops"><a href="read_topic.php?id=<?php echo $dnn2['id']; ?> " class="title"><?php echo htmlentities($dnn2['title'], ENT_QUOTES, 'UTF-8'); ?></a>
                        <p class="rel-position topic-meta">
                            by <a href="profile.php?id=<?php echo $dnn2['authorid']; ?>" ><?php echo htmlentities($dnn2['author'], ENT_QUOTES, 'UTF-8'); ?></a>. <?php echo $dnn2['replies']; ?> posts.
                            <?php
                            echo check_time($dnn2['timestamp']);

                        if(isset($_SESSION['username']) and ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1)) { ?>
                            <a href="delete_topic.php?id=<?php echo $dnn2['id']; ?>" class="abs-position" ><img src="<?php echo $design; ?>/images/delete.png" alt="Delete" /></a>
                        <?php
                        }?>
                        </p>
                        </td>
                    </tr>
                <?php
                }?>
                </table>
            <?php
            }else{ ?>
                <div class="message">This category has no topic.</div>
            <?php
            }
            if(!isset($_SESSION['username'])){ ?>
                <div class="box_login">
                	<form action="login.php" method="post">
                		<label for="username">Username</label><input type="text" name="username" id="username" /><br />
                		<label for="password">Password</label><input type="password" name="password" id="password" /><br />
                        <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" />
                        <div class="center">
                	        <input type="submit" value="Login" /> <input type="button" onclick="javascript:document.location='signup.php';" value="Sign Up" />
                        </div>
                    </form>
                </div>
            <?php
            }?>
                    <div class="content-footer">
                        <ul>
                            <li class="content-footer__item">
                                <?php echo pagination($statement,$limit,$page,$reload);?>
                            </li>
                            <?php
                            if(isset($_SESSION['username'])){ ?>
                            <li class="content-footer__item">
                                <a href="new_topic.php?parent=<?php echo $id; ?>">New Topic</a>
                            </li>
                            <?php
                            } ?>
                        </ul>
                    </div>
                </div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>
<?php
}else{
	echo '<h2>This category doesn\'t exist.</h2>';
}
}else{
	echo '<h2>The ID of the category you want to visit is not defined.</h2>';
}
?>