<?php
//This page let users create new topics
include('config.php');
include('user-right.php');
if(isset($_GET['parent']))
{
	$id = intval($_GET['parent']);
if(isset($_SESSION['username']))
{
	$dn1 = mysql_fetch_array(mysql_query('select count(c.id) as nb1, c.name from categories as c where c.id="'.$id.'"'));

if($dn1['nb1']>0)
{
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>New Topic - <?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?> - Forum</title>
		<script type="text/javascript" src="functions.js"></script>
    </head>
    <body>
    	<?php include_once ('header.php');?>
    	<div class="container">
    		<?php include ('ads.php'); ?>
    	</div>
        <div class="container content">
<div class="page-title page-breadcrumb">
	<ul>
		<li>
			<a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
		</li>
		<li>
			<a href="list_topics.php?parent=<?php echo $id; ?>">
				<?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?>
			</a>
		</li>
		<li>
			New Topic
		</li>
	</ul>
</div>
<div class="main-content">
<?php
if(isset($_POST['message'], $_POST['title']) and $_POST['message']!='' and $_POST['title']!=''){
	include('bbcode_function.php');
	$title = $_POST['title'];
	if(($_SESSION['username']==$admin || $_SESSION['perm'] >= 1) and isset($_POST['permission']) and $_POST['permission']!=''){
		$permission = $_POST['permission'];
	}else{
		$permission = 0;
	}
	$message = $_POST['message'];

	if(get_magic_quotes_gpc()){
		$title = stripslashes($title);
		$permission = stripslashes($permission);
		$message = stripslashes($message);
	}

	$title = mysql_real_escape_string($title);
	$permission = mysql_real_escape_string($permission);
	$message = mysql_real_escape_string(bbcode_to_html($message));

	if(mysql_query('insert into topics (parent, id, id2, title, message, permission, authorid, timestamp, timestamp2) select "'.$id.'", ifnull(max(id), 0)+1, "1", "'.$title.'", "'.$message.'", "'.$permission.'", "'.$_SESSION['userid'].'", "'.time().'", "'.time().'" from topics')){

		$tp1 = mysql_fetch_array(mysql_query('select MAX(t.id) as id from topics as t where t.id2="1" and t.parent="'.$id.'"'));

		if(isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])) {
      $status= array();
      $file_name = $_FILES['image']['name'];
      $file_size = $_FILES['image']['size'];
      $file_tmp = $_FILES['image']['tmp_name'];
      $file_type = $_FILES['image']['type'];
      $file_string = explode('.',$file_name);
      $file_ext = strtolower(end($file_string));
      $file_new_name = md5(uniqid(rand(), true)) .'.'. $file_ext;

      $expensions= array("jpeg","jpg","png","doc","docx","xlsx","ppt", "pptx", "pdf");

      if(in_array($file_ext,$expensions)=== false){
        $status="Extension not allowed, please choose a JPEG or PNG file.";

	      	if($file_size > 2097152) {
	         $status='File size must be excately 2 MB';
	      }
      }

      if(empty($status)==true) {
      	move_uploaded_file($file_tmp,"uploads/".$file_new_name);
      	$q_upload = mysql_query("INSERT INTO uploads (parent, id1, id2, authorid, real_name, new_name) VALUES ('".$id."', '".$tp1['id']."', '1', '".$_SESSION['userid']."', '".$file_name."', '".$file_new_name."' )");
      	$status="Success";
      }
   }
	?>
	<div class="message">The topic have successfully been created.
	<?php
		if (isset($status)) {
			echo "". $status;
		}
		header("refresh:3; list_topics.php?parent=" . $id);
	?>
	</div>
		<?php

	}else{
		echo 'An error occurred while creating the topic.';
	}
}
else
{
?>
<form action="new_topic.php?parent=<?php echo $id; ?>" method="post" enctype="multipart/form-data">
	<label for="title">Title</label><input type="text" name="title" id="title"  /><br />
	<?php
	if ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1) { ?>
		<label for="permission">Permission Level</label>
			<select id="permission" name="permission">
				<option value="0">Public</option>
				<option value="1">Member only</option>
				<option value="2">Admin Only</option>
			</select>
		<br />
	<?php
	}
	?>
    <label for="message">Message</label><br />
    <div class="message_buttons">
        <input type="button" value="Bold" onclick="javascript:insert('[b]', '[/b]', 'message');" /><!--
        --><input type="button" value="Italic" onclick="javascript:insert('[i]', '[/i]', 'message');" /><!--
        --><input type="button" value="Underlined" onclick="javascript:insert('[u]', '[/u]', 'message');" /><!--
        --><input type="button" value="Image" onclick="javascript:insert('[img]', '[/img]', 'message');" /><!--
        --><input type="button" value="Link" onclick="javascript:insert('[url]', '[/url]', 'message');" /><!--
        --><input type="button" value="Left" onclick="javascript:insert('[left]', '[/left]', 'message');" /><!--
        --><input type="button" value="Center" onclick="javascript:insert('[center]', '[/center]', 'message');" /><!--
        --><input type="button" value="Right" onclick="javascript:insert('[right]', '[/right]', 'message');" /><!--
        --><input type="button" value="Youtube" onclick="javascript:insert('[youtube]', '[/youtube]', 'message');" />
    </div>
    <textarea name="message" id="message" cols="70" rows="6"></textarea><br />
    <label> Attachment (jpeg, jpg, png, doc, docx, xlsx, ppt, pptx, pdf)
    <input type = "file" name = "image" /></label><br /><br />
    <input type="submit" value="Send" />
</form>
<?php
}
?>
	</div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>
<?php
}
else
{
	echo '<h2>The category you want to add a topic doesn\'t exist.</h2>';
}
}
else
{
?>
<h2>You must be logged to access this page.</h2>
<div class="box_login">
	<form action="login.php" method="post">
		<label for="username">Username</label><input type="text" name="username" id="username" /><br />
		<label for="password">Password</label><input type="password" name="password" id="password" /><br />
        <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" />
        <div class="center">
	        <input type="submit" value="Login" /> <input type="button" onclick="javascript:document.location='signup.php';" value="Sign Up" />
        </div>
    </form>
</div>
<?php
}
}
else
{
	echo '<h2>The ID of the category you want to add a topic is not defined.</h2>';
}
?>