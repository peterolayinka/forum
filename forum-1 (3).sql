-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 30, 2017 at 05:19 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `forum-1`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` smallint(6) NOT NULL,
  `sub_id` smallint(6) NOT NULL,
  `name` varchar(256) NOT NULL,
  `description` text NOT NULL,
  `position` smallint(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `sub_id`, `name`, `description`, `position`) VALUES
(1, 1, 'Nebosh', 'PECB 2017', 2),
(3, 2, 'Ibadan Zone', '', 1),
(4, 2, 'Port Harcourt Zone', '', 4),
(5, 1, 'General Discussion', '', 3),
(6, 3, 'Year', '', 5),
(7, 3, 'Year', '1', 6),
(8, 3, 'Year', '1', 7),
(9, 1, 'Year', 'Ths is a list by year of admission', 8),
(10, 2, 'Year', 'Ths is a list by year of admission', 9);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `parent` int(255) NOT NULL,
  `id1` int(255) NOT NULL,
  `id2` int(255) NOT NULL,
  `authorid` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `parent`, `id1`, `id2`, `authorid`) VALUES
(26, 3, 5, 1, 5),
(28, 3, 5, 2, 5),
(29, 3, 5, 2, 1),
(30, 3, 5, 1, 1),
(31, 3, 5, 3, 5),
(35, 4, 20, 1, 1),
(36, 4, 20, 2, 1),
(37, 4, 21, 1, 1),
(38, 4, 20, 2, 5),
(39, 4, 20, 1, 5),
(40, 3, 5, 4, 1),
(41, 3, 5, 11, 1),
(42, 3, 5, 12, 1),
(43, 4, 18, 11, 1),
(44, 4, 23, 1, 1),
(45, 1, 8, 32, 7);

-- --------------------------------------------------------

--
-- Table structure for table `pm`
--

CREATE TABLE IF NOT EXISTS `pm` (
  `pk` bigint(20) NOT NULL AUTO_INCREMENT,
  `id` bigint(20) NOT NULL,
  `id2` int(11) NOT NULL,
  `title` varchar(256) NOT NULL,
  `user1` bigint(20) NOT NULL,
  `user2` bigint(20) NOT NULL,
  `message` text NOT NULL,
  `timestamp` int(10) NOT NULL,
  `user1read` varchar(3) NOT NULL,
  `user2read` varchar(3) NOT NULL,
  PRIMARY KEY (`pk`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=56 ;

--
-- Dumping data for table `pm`
--

INSERT INTO `pm` (`pk`, `id`, `id2`, `title`, `user1`, `user2`, `message`, `timestamp`, `user1read`, `user2read`) VALUES
(1, 1, 1, 'Guess What???', 2, 1, 'Yes what up.', 1491435002, 'yes', 'yes'),
(2, 1, 2, '', 1, 0, 'Hello, <br />\r\n<br />\r\nWhat can i help you with', 1491435049, '', ''),
(3, 1, 3, '', 1, 0, '??', 1491435888, '', ''),
(4, 1, 4, '', 1, 0, 'Am waiting', 1491435899, '', ''),
(5, 1, 5, '', 1, 0, 'yes', 1491666270, '', ''),
(6, 1, 6, '', 2, 0, 'helo<br />\r\n', 1491666619, '', ''),
(7, 1, 7, '', 1, 0, 'Hello', 1491666652, '', ''),
(8, 8, 1, 'Please reply', 2, 1, 'Please, i Want to ask just one question', 1491666726, 'yes', 'yes'),
(9, 1, 8, '', 2, 0, 'am with you', 1491666751, '', ''),
(10, 8, 2, '', 1, 0, 'Hello @yeanke, Please go on. How may i Help you.', 1491667410, '', ''),
(11, 1, 9, '', 1, 0, 'please, i need to learn some skills', 1491690956, '', ''),
(12, 1, 10, '', 1, 0, 'can you teach me please?', 1491690973, '', ''),
(13, 1, 11, '', 1, 0, 'i am ready to learn', 1491690985, '', ''),
(14, 1, 12, '', 1, 0, 'hello, you there', 1492167357, '', ''),
(15, 1, 13, '', 1, 0, 'hello, you there', 1492167683, '', ''),
(16, 16, 1, 'I am currently testing', 1, 2, 'Heyo, kilon hapun.', 1492168508, 'yes', 'no'),
(17, 17, 1, 'still Testin', 1, 2, 'testing, still testing!...', 1492168870, 'yes', 'yes'),
(18, 18, 1, 'Helo', 2, 1, 'Helo what s really happening up here', 1492470798, 'yes', 'yes'),
(19, 19, 1, 'hey, respond jawe', 2, 1, 'hey man, i am testing this pagination feature, dont mind it', 1492470853, 'yes', 'yes'),
(36, 24, 1, 'Welcome on Board!..', 1, 2, 'Hello People', 1492958149, 'yes', 'no'),
(35, 23, 1, 'Welcome on Board!..', 1, 0, 'Hello People', 1492958049, 'yes', 'no'),
(34, 22, 1, 'Welcome on Board!..', 1, 0, 'Hello People', 1492958049, 'yes', 'no'),
(33, 21, 1, 'Welcome on Board!..', 1, 0, 'Hello People', 1492958049, 'yes', 'no'),
(32, 20, 1, 'Welcome on Board!..', 1, 2, 'Hello People', 1492958049, 'yes', 'no'),
(37, 25, 1, 'Welcome on Board!..', 1, 5, 'Hello People', 1492958149, 'yes', 'no'),
(38, 26, 1, 'Welcome on Board!..', 1, 4, 'Hello People', 1492958149, 'yes', 'no'),
(39, 27, 1, 'Welcome on Board!..', 1, 3, 'Hello People', 1492958149, 'yes', 'no'),
(40, 28, 1, 'Welcome on Board!..', 1, 2, 'Hello People', 1492979270, 'yes', 'no'),
(41, 29, 1, 'Welcome on Board!..', 1, 5, 'Hello People', 1492979270, 'yes', 'no'),
(42, 30, 1, 'Welcome on Board!..', 1, 4, 'Hello People', 1492979270, 'yes', 'no'),
(43, 31, 1, 'Welcome on Board!..', 1, 3, 'Hello People', 1492979270, 'yes', 'no'),
(44, 32, 1, 'Attachement Test', 1, 2, 'Hello, Yeanke. Check out my attachment', 1493517270, 'yes', 'no'),
(45, 33, 1, 'Attachement Test', 1, 2, 'Hello, Yeanke. Check out my attachment', 1493517417, 'yes', 'yes'),
(46, 34, 1, 'Attachement Test', 1, 2, 'Hello, Yeanke. Check out my attachment', 1493517583, 'yes', 'no'),
(47, 35, 1, 'Attachement Test', 1, 2, 'Hello, Yeanke. Check out my attachment', 1493517641, 'yes', 'yes'),
(48, 35, 2, '', 1, 0, 'Hello', 1493520697, '', ''),
(49, 35, 3, '', 1, 0, 'hey', 1493520804, '', ''),
(50, 35, 4, '', 1, 0, 'hey', 1493520857, '', ''),
(51, 35, 5, '', 1, 0, 'hey', 1493520905, '', ''),
(53, 35, 7, '', 2, 0, 'hello', 1493521126, '', ''),
(52, 35, 6, '', 2, 0, 'hi', 1493521024, '', ''),
(54, 35, 8, '', 2, 0, 'Hey', 1493521167, '', ''),
(55, 35, 9, '', 2, 0, 'hey, Look at me now', 1493521519, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pm_uploads`
--

CREATE TABLE IF NOT EXISTS `pm_uploads` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `real_name` varchar(255) NOT NULL,
  `new_name` varchar(255) NOT NULL,
  `id1` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `pm_uploads`
--

INSERT INTO `pm_uploads` (`id`, `real_name`, `new_name`, `id1`, `id2`) VALUES
(1, '0', '0', 33, 1),
(2, '0', '7', 34, 1),
(3, '_Â®Â²ÂºÂ¹3.jpg', 'aa5e76a1495b04e51c8ada49106e3c5a.jpg', 35, 1),
(4, '~_.jpg', '4e3ac7fae6db8c00fce5f084f8a61545.jpg', 35, 4),
(5, '~_.jpg', 'bae68a5bb7cc0f8c74a605c71414e2bc.jpg', 35, 5),
(6, '_UNTICK_ Mr J Wyld _3 Miss N Wyld Too Be _3.JPEG', '6c656c8029a10e7e113cf5e41ba420fc.jpeg', 35, 7),
(7, 'A WELCOME ADDRESS.pptx', '5d15e96d2eeaf7a9924271732b5e4035.pptx', 35, 8),
(8, 'conclusion(siwes).jpg', '938f1f0fc9e3293569af4ba99354a295.jpg', 35, 9);

-- --------------------------------------------------------

--
-- Table structure for table `quote`
--

CREATE TABLE IF NOT EXISTS `quote` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `id1` int(255) NOT NULL,
  `id2` int(11) NOT NULL,
  `parent` int(11) NOT NULL,
  `user_id` int(255) NOT NULL,
  `message` varchar(255) NOT NULL,
  `time_stamp` int(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ads1_link` varchar(255) DEFAULT 'none',
  `ads1_image_url` varchar(255) DEFAULT 'none',
  `ads2_link` varchar(255) DEFAULT 'none',
  `ads2_image_url` varchar(255) DEFAULT 'none',
  `ads3_link` varchar(255) DEFAULT 'none',
  `ads3_image_url` varchar(255) DEFAULT 'none',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `ads1_link`, `ads1_image_url`, `ads2_link`, `ads2_image_url`, `ads3_link`, `ads3_image_url`) VALUES
(1, '#', 'default/images/ad1.jpg', '#', 'default/images/ad-placeholder.jpg', '#', 'default/images/ad-placeholder.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `parent` smallint(6) NOT NULL,
  `id` int(11) NOT NULL,
  `id2` int(11) NOT NULL,
  `frontpage` tinyint(1) DEFAULT NULL,
  `front_date` int(11) DEFAULT NULL,
  `title` varchar(256) NOT NULL,
  `message` longtext NOT NULL,
  `authorid` int(11) NOT NULL,
  `timestamp` int(11) NOT NULL,
  `timestamp2` int(11) NOT NULL,
  `permission` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`,`id2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `topics`
--

INSERT INTO `topics` (`parent`, `id`, `id2`, `frontpage`, `front_date`, `title`, `message`, `authorid`, `timestamp`, `timestamp2`, `permission`) VALUES
(1, 1, 1, 1, 1491999665, 'Best before date:', '<br />\r\nFree sites have a limited lifespan, but you can renew that here up to a maximum of three months from today&#039;s date. You can always extend it later too! We&#039;ll send you an email a week before it expires. See here for more details.<br />\r\n<br />\r\nThis site will be disabled on Tuesday 04 July 2017<br />\r\n<br />\r\n Run until 3 months from today<br />\r\nPaying users&#039; sites do not have expiry dates.<br />\r\n<br />\r\nTraffic:<br />\r\nHow busy is your site?<br />\r\n<br />\r\nThis month (previous month)	2	(0)<br />\r\nToday (yesterday)	2	(0)<br />\r\nHour (previous hour)	2	(0)<br />\r\nWant some more data? Paying accounts get pretty charts ;-)<br />\r\n<br />\r\nCode:<br />\r\nWhat your site is running.<br />\r\n<br />\r\nSource code:<br />\r\nEnter the path to your web app source code<br />\r\nWorking directory:<br />\r\n/home/peterolayinka/<br />\r\nGo to directory<br />\r\nWSGI configuration file:/var/www/peterolayinka_pythonanywhere_com_wsgi.py<br />\r\nPython version:3.4<br />\r\nVirtualenv:<br />\r\nUse a virtualenv to get different versions of flask, django etc from our default system ones. More info here. You need to Reload your web app to activate it; NB - will do nothing if the virtualenv does not exist.<br />\r\n<br />\r\n/home/peterolayinka/blog/myvenv<br />\r\n<br />\r\n Start a console in this virtualenv<br />\r\n<br />\r\nLog files:<br />\r\nThe first place to look if something goes wrong.<br />\r\n<br />\r\nAccess log:peterolayinka.pythonanywhere.com.access.log<br />\r\nError log:peterolayinka.pythonanywhere.com.error.log<br />\r\nServer log:peterolayinka.pythonanywhere.com.server.log<br />\r\nLog files are periodically rotated. You can find old logs here: /var/log<br />\r\n<br />\r\nStatic files:<br />\r\nFiles that aren&#039;t dynamically generated by your code, like CSS, JavaScript or uploaded files, can be served much faster straight off the disk if you specify them here. You need to Reload your web app to activate any changes you make to the mappings below.<br />\r\n<br />\r\nURL	Directory	Delete<br />\r\nEnter URL	Enter path	<br />\r\nPassword protection:<br />\r\nIdeal for sites that are under development when you don&#039;t want anyone to see them yet. You need to Reload your web app to activate any changes made here.<br />\r\n<br />\r\nPassword protection:Enabled Disabled<br />\r\nUsername:Enter a username<br />\r\nPassword:Enter a password', 1, 1491430954, 1492982500, 2),
(1, 1, 2, NULL, 2147483647, '', 'hey, what do you mean', 1, 1491431004, 1491431004, 0),
(1, 1, 3, NULL, 2147483647, '', 'Hello, Please i don&#039;t get you.', 2, 1491431837, 1491431837, 0),
(3, 5, 1, 1, 1491931035, 'Nebosh Class 2017', 'This is nebosh fresh class 2017! yes', 1, 1491863600, 1493494055, 2),
(1, 4, 1, 1, 1491930456, 'Ibadan Zone', 'This arena is for the people in ibadan.', 1, 1491521379, 1491521379, 0),
(3, 5, 2, NULL, NULL, '', 'This is a test comment<br />\r\n', 2, 1491931106, 1491931106, 0),
(4, 6, 1, 1, 1492045642, 'Nice being here', 'Cadosh is the name of the people here', 2, 1491931217, 1491931217, 0),
(1, 7, 1, NULL, NULL, 'ibadan 2017', 'This is ibadan set 2017', 1, 1491942830, 1491942830, 0),
(1, 8, 1, 1, 1492071858, 'Another nebosh set', 'New nebosh set', 1, 1491943900, 1493500993, 0),
(1, 9, 1, 0, 1492000243, 'this is a text topic', 'This topic is fro text', 1, 1491995874, 1491995874, 0),
(4, 10, 1, 1, 1492045630, 'Test 1', 'testing', 1, 1492045618, 1492045618, 0),
(4, 11, 1, 1, 1492045671, 'texinh', 'testingtesting testing', 1, 1492045660, 1492045660, 0),
(3, 12, 1, 1, 1492045821, 'Another ibadan post', 'yes', 1, 1492045814, 1492045814, 0),
(3, 13, 1, 1, 1492045850, 'ibadan 2017', 'yes', 1, 1492045841, 1492045841, 0),
(3, 14, 1, 1, 1492045875, 'ibadan 2016', 'ibadan 2016', 1, 1492045868, 1492045868, 0),
(3, 15, 1, 1, 1492045900, 'ibadan 2015', 'ibadan 2016', 1, 1492045892, 1492045892, 0),
(1, 8, 2, NULL, NULL, '', 'Hello, anybody here.', 1, 1492074738, 1492074738, 0),
(1, 8, 3, NULL, NULL, '', 'Hello', 1, 1492074846, 1492074846, 0),
(1, 8, 4, NULL, NULL, '', 'hey', 1, 1492074925, 1492074925, 0),
(1, 8, 5, NULL, NULL, '', 'hello', 1, 1492470974, 1492470974, 0),
(1, 8, 6, NULL, NULL, '', 'Hey', 1, 1492471135, 1492471135, 0),
(1, 1, 4, NULL, NULL, '', 'you mean', 1, 1492982499, 1492982499, 0),
(3, 5, 3, NULL, NULL, '', 'Hello admin panel', 1, 1492982914, 1492982914, 0),
(4, 16, 1, NULL, NULL, 'xkjf,', ',mn', 1, 1492984063, 1492984063, 0),
(4, 17, 1, NULL, NULL, 'them', 'bhjdf', 1, 1492984224, 1492984224, 0),
(4, 18, 1, NULL, NULL, 'Admin Only', 'Hello, this is admin page.', 1, 1492984408, 1493515475, 2),
(4, 19, 1, NULL, NULL, 'Are you all great', 'yes, surly.', 5, 1492984760, 1492984760, 0),
(4, 20, 1, NULL, NULL, 'User Only Post', 'hhbjkfdbvjnskdvbjfnsj,j,f nvljnx;om ;ldjn;ldvml;bjcn .kxfmdlbmd;mnlfmo;ntem', 1, 1492984832, 1492984954, 1),
(4, 20, 2, NULL, NULL, '', 'yello\\w', 1, 1492984954, 1492984954, 0),
(4, 18, 2, NULL, NULL, '', 'hello fellow admin', 1, 1493072884, 1493072884, 0),
(4, 21, 1, NULL, NULL, 'Guest allowed', 'Hello peeps this is a post for guests<img src="http://localhost/sandbox/2017/Forum/default/images/ad1.jpg" alt="Image" />', 1, 1493073122, 1493073122, 0),
(3, 5, 4, NULL, NULL, '', '<strong>This is a test comment&lt;br /&gt;</strong><br />\r\n', 1, 1493328497, 1493328497, 0),
(3, 5, 5, NULL, NULL, '', '<q>Hello admin panel</q><br>', 1, 1493328831, 1493328831, 0),
(3, 5, 11, NULL, NULL, '', '<q>This is nebosh fresh class 2017 <strong>#peterolayinka</strong></q><br />\r\n<img src="http://localhost/sandbox/2017/Forum/default/images/logo.png" alt="Image" /><br />\r\nwhat is all this about self.', 1, 1493341480, 1493341480, 0),
(3, 5, 7, NULL, NULL, '', 'Hello admin panel', 1, 1493329295, 1493329295, 0),
(1, 8, 8, NULL, NULL, '', 'Hello', 1, 1493453007, 1493453007, 0),
(1, 8, 9, NULL, NULL, '', 'yes boss', 1, 1493453248, 1493453248, 0),
(3, 5, 10, NULL, NULL, '', '<q>Hello admin panel<br> <strong>#peterolayinka</strong><br />\r\n<br />\r\nwhat is goin on here admin, how can i help you?<br> let me know. ASAP!!! <strong>#peterolayinka</strong><br />\r\n<br />\r\ni am still waiting for you response o. <strong>#peterolayinka</strong></q><br />\r\n<br />\r\n<img src="http://localhost/sandbox/2017/Forum/default/images/logo.png" alt="Image" />', 1, 1493341416, 1493341416, 0),
(1, 8, 7, NULL, NULL, '', 'Hello Sir', 1, 1493452850, 1493452850, 0),
(3, 5, 13, NULL, NULL, '', '<iframe class="youtube" src="https://www.youtube.com/embed/dv4OzOtJxQI" frameborder="0"></iframe>', 1, 1493412437, 1493412437, 0),
(3, 5, 12, NULL, NULL, '', 'Post Deleted', 1, 1493410513, 1493410513, 0),
(3, 5, 8, NULL, NULL, '', '<q>Hello admin panel<br> <strong>#peterolayinka</strong></q><br />\r\n<br />\r\nwhat is goin on here admin, how can i help you?<br> let me know. ASAP!!!', 1, 1493341306, 1493341306, 0),
(3, 5, 9, NULL, NULL, '', '<q>Hello admin panel<br> <strong>#peterolayinka</strong><br />\r\n<br />\r\nwhat is goin on here admin, how can i help you?<br> let me know. ASAP!!! <strong>#peterolayinka</strong></q><br />\r\n<br />\r\ni am still waiting for you response o.', 1, 1493341363, 1493341363, 0),
(1, 8, 10, NULL, NULL, '', 'hello boss', 1, 1493453317, 1493453317, 0),
(1, 8, 11, NULL, NULL, '', 'Yes Boss', 1, 1493453742, 1493453742, 0),
(1, 8, 12, NULL, NULL, '', 'yes', 1, 1493453897, 1493453897, 0),
(1, 8, 13, NULL, NULL, '', 'yes', 1, 1493453952, 1493453952, 0),
(1, 8, 14, NULL, NULL, '', 'yes', 1, 1493454119, 1493454119, 0),
(1, 8, 15, NULL, NULL, '', 'yes', 1, 1493454198, 1493454198, 0),
(1, 8, 16, NULL, NULL, '', 'yes', 1, 1493454305, 1493454305, 0),
(1, 8, 17, NULL, NULL, '', 'yes', 1, 1493454420, 1493454420, 0),
(1, 8, 18, NULL, NULL, '', 'yes', 1, 1493454427, 1493454427, 0),
(1, 8, 19, NULL, NULL, '', 'yes', 1, 1493454538, 1493454538, 0),
(1, 8, 20, NULL, NULL, '', 'yes', 1, 1493454670, 1493454670, 0),
(1, 8, 21, NULL, NULL, '', 'yes', 1, 1493454709, 1493454709, 0),
(1, 8, 22, NULL, NULL, '', 'yes', 1, 1493454778, 1493454778, 0),
(1, 8, 23, NULL, NULL, '', 'yes', 1, 1493454781, 1493454781, 0),
(1, 8, 24, NULL, NULL, '', 'yes', 1, 1493454795, 1493454795, 0),
(1, 8, 25, NULL, NULL, '', 'yes', 1, 1493454842, 1493454842, 0),
(1, 8, 26, NULL, NULL, '', 'Hey Bro', 1, 1493454862, 1493454862, 0),
(1, 8, 27, NULL, NULL, '', 'Hey Bro', 1, 1493455160, 1493455160, 0),
(1, 8, 28, NULL, NULL, '', 'Hey Bro', 1, 1493455209, 1493455209, 0),
(1, 8, 29, NULL, NULL, '', 'Hey Bro', 1, 1493455460, 1493455460, 0),
(1, 8, 30, NULL, NULL, '', 'Hey Bro', 1, 1493455548, 1493455548, 0),
(1, 8, 31, NULL, NULL, '', 'Hey Bro', 1, 1493455769, 1493455769, 0),
(4, 18, 3, NULL, NULL, '', 'hello admin', 1, 1493457071, 1493457071, 0),
(1, 8, 32, NULL, NULL, '', 'Hey Bro', 1, 1493457482, 1493457482, 0),
(4, 18, 4, NULL, NULL, '', 'Helo', 1, 1493457517, 1493457517, 0),
(4, 18, 5, NULL, NULL, '', 'yes, am here', 1, 1493457915, 1493457915, 0),
(4, 18, 6, NULL, NULL, '', 'yes, am here', 1, 1493458151, 1493458151, 0),
(4, 18, 7, NULL, NULL, '', 'yes, am here', 1, 1493458190, 1493458190, 0),
(4, 18, 8, NULL, NULL, '', 'yes, am here', 1, 1493458221, 1493458221, 0),
(4, 18, 9, NULL, NULL, '', 'yes, am here', 1, 1493458464, 1493458464, 0),
(4, 18, 10, NULL, NULL, '', 'Hello, check this internship doc out.', 1, 1493459644, 1493459644, 0),
(4, 18, 11, NULL, NULL, '', 'Hello, check this internship doc out.', 1, 1493459695, 1493459695, 0),
(4, 18, 12, NULL, NULL, '', 'Hello, What up', 1, 1493460062, 1493460062, 0),
(4, 18, 13, NULL, NULL, '', 'yes, am here', 1, 1493461421, 1493461421, 0),
(4, 18, 14, NULL, NULL, '', 'yes, am here', 1, 1493461448, 1493461448, 0),
(4, 18, 15, NULL, NULL, '', 'yes, am here', 1, 1493461478, 1493461478, 0),
(4, 18, 16, NULL, NULL, '', 'yes, am here', 1, 1493461494, 1493461494, 0),
(4, 18, 17, NULL, NULL, '', 'hello', 1, 1493492928, 1493492928, 0),
(3, 5, 14, NULL, NULL, '', '<iframe class="youtube" src="https://www.youtube.com/embed/dv4OzOtJxQI" frameborder="0"></iframe>', 1, 1493494055, 1493494055, 0),
(4, 22, 1, NULL, NULL, 'Test attachment', '<iframe class="youtube" src="https://www.youtube.com/embed/dv4OzOtJxQI" frameborder="0"></iframe>', 1, 1493494098, 1493494098, 0),
(4, 23, 1, NULL, NULL, 'Test 2', '<iframe class="youtube" src="https://www.youtube.com/embed/dv4OzOtJxQI" frameborder="0"></iframe>', 1, 1493494336, 1493494684, 0),
(4, 23, 2, NULL, NULL, '', '<q><iframe class="youtube" src="https://www.youtube.com/embed/dv4OzOtJxQI" frameborder="0"></iframe> <strong>#peterolayinka</strong></q>', 1, 1493494684, 1493494684, 0),
(4, 24, 1, NULL, NULL, 'Admin Test ', 'hey', 1, 1493494844, 1493494844, 2),
(4, 25, 1, NULL, NULL, 'Public Test 2', 'Hello Public.', 1, 1493495415, 1493508667, 0),
(4, 26, 1, NULL, NULL, 'Admin Test 2', 'Hello', 1, 1493496511, 1493496511, 2),
(1, 8, 33, NULL, NULL, '', '<q>Hey Bro <strong>#peterolayinka</strong></q>i quoted him', 7, 1493500993, 1493500993, 0),
(4, 25, 2, NULL, NULL, '', 'hello', 1, 1493508667, 1493508667, 0),
(4, 18, 18, NULL, NULL, '', 'yes boss', 1, 1493510363, 1493510363, 0),
(4, 18, 19, NULL, NULL, '', 'hello', 1, 1493510383, 1493510383, 0),
(4, 18, 20, NULL, NULL, '', 'jhgh', 1, 1493510513, 1493510513, 0),
(4, 18, 21, NULL, NULL, '', 'Hello', 1, 1493510587, 1493510587, 0),
(4, 18, 22, NULL, NULL, '', 'yes i see', 1, 1493510629, 1493510629, 0),
(4, 18, 23, NULL, NULL, '', '<q>yes i see <strong>#peterolayinka</strong></q>', 1, 1493515270, 1493515270, 0),
(4, 18, 24, NULL, NULL, '', '<q>yes boss <strong>#peterolayinka</strong></q><br />\r\ni caught you sir.', 1, 1493515475, 1493515475, 0),
(4, 27, 1, NULL, NULL, 'Yes it worked', 'Hello, testing', 1, 1493515552, 1493515552, 0),
(4, 28, 1, NULL, NULL, 'still checking things out', 'yes ', 1, 1493515704, 1493515704, 0),
(4, 29, 1, NULL, NULL, 'still checking things out', 'jhklsf', 1, 1493515772, 1493515772, 0),
(4, 30, 1, NULL, NULL, 'This is a pub post', 'Hello, Please check this out', 1, 1493516597, 1493516597, 0);

-- --------------------------------------------------------

--
-- Table structure for table `uploads`
--

CREATE TABLE IF NOT EXISTS `uploads` (
  `id` int(255) NOT NULL AUTO_INCREMENT,
  `parent` int(255) NOT NULL,
  `id1` int(255) NOT NULL,
  `id2` int(255) NOT NULL,
  `authorid` int(255) NOT NULL,
  `real_name` varchar(255) NOT NULL,
  `new_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=25 ;

--
-- Dumping data for table `uploads`
--

INSERT INTO `uploads` (`id`, `parent`, `id1`, `id2`, `authorid`, `real_name`, `new_name`) VALUES
(1, 1, 8, 0, 1, '', '17619f4329d7b394f5b38b4b89e6ec75.jpg'),
(2, 1, 8, 0, 1, '', '427a9ece5907d3b76499f3fbce1f7dc6.jpg'),
(3, 1, 8, 1, 1, 'IMG_20161112_101843.jpg', '90c4887aab5ad78cf70f9faee45157a1.jpg'),
(4, 4, 18, 7, 1, 'IMG_20161112_084038.jpg', '20cb7bd7a43c68c0bd9a1c6ec5136004.jpg'),
(5, 1, 8, 1, 1, 'IMG_20161112_101843.jpg', 'e2b6d8a37aea2bbc3fc8cb6ef89e8dcd.jpg'),
(6, 4, 18, 5, 1, 'IMG_20161112_083102.jpg', '091c99ab951cd1d6bc8ed40214381503.jpg'),
(7, 4, 18, 6, 1, 'IMG_20161112_101427.jpg', 'b79e0273d97e039cef9646a9674ad66e.jpg'),
(8, 4, 18, 11, 1, 'Effective Internship Program.pdf', 'cb79fafa9adf089cfb4c511fb3ee4f58.pdf'),
(9, 4, 18, 12, 1, 'EKSU.png', '039783c28274e7bedca30f11b2694521.png'),
(10, 4, 18, 15, 1, 'IMG_20161112_101427.jpg', '91187b89ee72fe07e889de9999c277f3.jpg'),
(11, 4, 18, 16, 1, 'Â£f.jpg', '93f9ed35e0fa96654e809af8f1bc7d36.jpg'),
(12, 4, 18, 17, 1, 'PASTOR FUNSHO SEMINAR.pptx', 'a8d6ac80a1bc3a954547611c8b7c9909.pptx'),
(13, 3, 5, 14, 1, 'IMG_20160415_123031.jpg', 'e6df4568fcb33ba2e9f5505197ffb159.jpg'),
(14, 4, 22, 1, 1, '13000092_10207978443850852_1032148390875474167_n.jpg', '348899fc9e734e7a0d96c04d485442ba.jpg'),
(15, 4, 23, 1, 1, '13000092_10207978443850852_1032148390875474167_n.jpg', '28d055ad379b112ac8d3ff2005b10139.jpg'),
(16, 4, 24, 1, 1, 'EKSU.png', '00a3dc66c23051d6e7eb08de019b27ce.png'),
(17, 4, 25, 1, 1, '13000092_10207978443850852_1032148390875474167_n.jpg', '59618caacb9f4b0953b89e19bce1b89d.jpg'),
(18, 4, 26, 1, 1, 'EKSU.png', 'f9835095e4a0b5f35c00d58f5e09b6f9.png'),
(19, 4, 25, 2, 1, 'EKSU.png', '70bbd1a56aab3b88d38dbd9a290875c4.png'),
(20, 4, 18, 22, 1, '(y)Y o.jpg', 'a4a04a7fc440a2e9e26c04c9ff289256.jpg'),
(21, 4, 0, 0, 1, 'Â£f.jpg', '0d0c51eebab395fa956b3157cccfda94.jpg'),
(22, 4, 18, 24, 1, '@.jpg', '53d40667ceb737dbdb0128bbf9b695b5.jpg'),
(23, 4, 0, 1, 1, '_UNTICK_ Mr J Wyld _3 Miss N Wyld Too Be _3.JPEG', 'e87c8a72c58f561df9cb1638fac1375b.jpeg'),
(24, 4, 30, 1, 1, '_UNTICK_ Mr J Wyld _3 Miss N Wyld Too Be _3.JPEG', '39a5da92142d96df971ef85d7ad58840.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `avatar` text NOT NULL,
  `signup_date` int(10) NOT NULL,
  `dob` int(10) NOT NULL,
  `gender` varchar(3) NOT NULL,
  `ban` tinyint(1) NOT NULL DEFAULT '0',
  `perm_level` tinyint(2) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `avatar`, `signup_date`, `dob`, `gender`, `ban`, `perm_level`) VALUES
(1, 'peterolayinka', '93ee6a37b476a415089a2cb4061701bb1d04a32d', 'peterolayinka97@gmail.com', 'http://localhost/sandbox/2017/Forum/default/images/new.jpg', 1491430667, 1032562800, 'm', 0, 2),
(2, 'yeanke', '93ee6a37b476a415089a2cb4061701bb1d04a32d', 'peterolayinka97@yahoo.com', 'http://localhost/sandbox/2017/Forum/default/images/yeanke.jpg', 1491431785, 0, 'm', 0, 1),
(3, 'Novelle', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'peter.softzest@gmail.com', '', 1491697747, 0, 'm', 0, 1),
(4, 'tony', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'peterolayinka97@gmail.com', '20, olufunmilayo str, ikosi ketu lagos.', 1492874755, 924735600, 'm', 0, 0),
(5, 'Shile', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'peterolayinka97@gmail.com', '', 1492876323, 1082588400, 'f', 0, 0),
(6, 'User1', '5baa61e4c9b93f3f0682250b6cf8331b7ee68fd8', 'peterolayinka97@gmail.com', '', 1493033815, 1051138800, 'm', 0, 0),
(7, 'water', '7c4a8d09ca3762af61e59520943dc26494f8941b', 'ol@email.com', '', 1493500819, 736210800, 'm', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
