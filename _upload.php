<?php
   if(isset($_FILES['image'])){
      $errors= array();
      $file_name = $_FILES['image']['name'];
      $file_size = $_FILES['image']['size'];
      $file_tmp = $_FILES['image']['tmp_name'];
      $file_type = $_FILES['image']['type'];
      $file_string = explode('.',$file_name);
      $file_ext = strtolower(end($file_string));
      $file_new_name = md5(uniqid(rand(), true)) .'.'. $file_ext;

      $expensions= array("jpeg","jpg","png");

      if(in_array($file_ext,$expensions)=== false){
        $errors="extension not allowed, please choose a JPEG or PNG file.";

	      	if($file_size > 2097152) {
	         $errors='File size must be excately 2 MB';
	      }
      }

      if(empty($errors)==true) {
      	move_uploaded_file($file_tmp,"uploads/".$file_name);
      	echo "Success";
      }else{
        echo $errors;
      }
   }
?>
<html>
   <body>

      <form action = "" method = "POST" enctype = "multipart/form-data">
         <input type = "file" name = "image" />
         <input type = "submit"/>

         <ul>
            <li>Sent file: <?php echo $file_name;  ?>
            <li>New Name: <?php echo $file_new_name;  ?>
            <li>File size: <?php echo $_FILES['image']['size'];  ?>
            <li>File type: <?php echo $_FILES['image']['type'] ?>
         </ul>

      </form>

   </body>
</html>