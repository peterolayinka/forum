<?php
//This page displays the list of the forum's categories
include('config.php');

$page = (int) (!isset($_GET["page"]) ? 1 : $_GET["page"]);

$startpoint = ($page * $limit) - $limit;

$statement = "topics as t where t.id2=1 and t.frontpage=1";

?>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.9";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Forum</title>
    </head>
    <body class="index">
    	<?php include_once ('header.php'); ?>
        <div class="container content">

			<div class="forums-box">
				<h3>Alumni Community <?php
			if(isset($_SESSION['username']) and ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1)){ ?>
				<a href="new_category.php" class="add-new">(New)</a>
			<?php
			}?></h3>
				<div class="forum-list">
					<ul>
					<li class="forum-item"> General Discussion: </li>
					<?php
						$dn1 = mysql_query('select c.id, c.name, c.description, c.sub_id, c.position, (select count(t.id) from topics as t where t.parent=c.id and t.id2=1) as topics, (select count(t2.id) from topics as t2 where t2.parent=c.id and t2.id2!=1) as replies from categories as c where c.sub_id=1 group by c.id order by c.position asc');
						$nb_cats = mysql_num_rows($dn1);

						while($dnn1 = mysql_fetch_array($dn1)) { ?>
							<li class="forum-item">
							<a href="list_topics.php?parent=<?php echo $dnn1['id']; ?>" class="title"><?php echo htmlentities($dnn1['name'], ENT_QUOTES, 'UTF-8'); ?></a>
							<?php
							if(isset($_SESSION['username']) and ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1)) { ?>
							    	<a href="delete_category.php?id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/delete.png" alt="Delete" /></a>
									<?php if($dnn1['position']>1){ ?><a href="move_category.php?action=up&id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/up.png" alt="Move Up" /></a><?php } ?>
									<?php if($dnn1['position']<$nb_cats){ ?><a href="move_category.php?action=down&id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/down.png" alt="Move Down" /></a><?php } ?>
									<a href="edit_category.php?id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/edit.png" alt="Edit" /></a>
							</li>
						<?php
							}

						}?>
					</ul>
				</div>
				<div class="forum-list">
					<ul>
						<li class="forum-item"> Careers: </li>
					<?php
						$dn1 = mysql_query('select c.id, c.name, c.description, c.sub_id, c.position, (select count(t.id) from topics as t where t.parent=c.id and t.id2=1) as topics, (select count(t2.id) from topics as t2 where t2.parent=c.id and t2.id2!=1) as replies from categories as c where c.sub_id=2 group by c.id order by c.position asc');
						$nb_cats = mysql_num_rows($dn1);

						while($dnn1 = mysql_fetch_array($dn1)) { ?>
							<li class="forum-item">
							<a href="list_topics.php?parent=<?php echo $dnn1['id']; ?>" class="title"><?php echo htmlentities($dnn1['name'], ENT_QUOTES, 'UTF-8'); ?></a>
							<?php
							if(isset($_SESSION['username']) and ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1)) { ?>
							    	<a href="delete_category.php?id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/delete.png" alt="Delete" /></a>
									<?php if($dnn1['position']>1){ ?><a href="move_category.php?action=up&id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/up.png" alt="Move Up" /></a><?php } ?>
									<?php if($dnn1['position']<$nb_cats){ ?><a href="move_category.php?action=down&id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/down.png" alt="Move Down" /></a><?php } ?>
									<a href="edit_category.php?id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/edit.png" alt="Edit" /></a>
							</li>
						<?php
							}

						}?>
					</ul>
				</div>
				<div class="forum-list">
					<ul>
						<li class="forum-item"> Training Forum: </li>
					<?php
						$dn1 = mysql_query('select c.id, c.name, c.description, c.sub_id, c.position, (select count(t.id) from topics as t where t.parent=c.id and t.id2=1) as topics, (select count(t2.id) from topics as t2 where t2.parent=c.id and t2.id2!=1) as replies from categories as c where c.sub_id=3 group by c.id order by c.position asc');
						$nb_cats = mysql_num_rows($dn1);

						while($dnn1 = mysql_fetch_array($dn1)) { ?>
							<li class="forum-item">
							<a href="list_topics.php?parent=<?php echo $dnn1['id']; ?>" class="title"><?php echo htmlentities($dnn1['name'], ENT_QUOTES, 'UTF-8'); ?></a>
							<?php
							if(isset($_SESSION['username']) and ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1)) { ?>
							    	<a href="delete_category.php?id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/delete.png" alt="Delete" /></a>
									<?php if($dnn1['position']>1){ ?><a href="move_category.php?action=up&id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/up.png" alt="Move Up" /></a><?php } ?>
									<?php if($dnn1['position']<$nb_cats){ ?><a href="move_category.php?action=down&id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/down.png" alt="Move Down" /></a><?php } ?>
									<a href="edit_category.php?id=<?php echo $dnn1['id']; ?>"><img src="<?php echo $design; ?>/images/edit.png" alt="Edit" /></a>
							</li>
						<?php
							}

						}?>
					</ul>
				</div>
			</div>
			<?php include ('ads.php'); ?>
			<ul class="index-list topic_table">
			<?php
			$dn2 = mysql_query("SELECT t.title, t.id from {$statement} group by t.id order by t.front_date desc LIMIT {$startpoint} , {$limit}");
			$nb_topic = mysql_num_rows($dn2);
			?>
				<li>
					<ul class="index-title">
						<li>
							<a href="https://web.facebook.com/NovelleCenter/">Facebook</a>
						</li>
						<li>
							<a href="#"> Twitter</a>
						</li>
					</ul>
				</li>
				<?php
			while($dnn2 = mysql_fetch_array($dn2)) { ?>
				<li class="forum_topic">
			    	<a href="read_topic.php?id=<?php echo $dnn2['id']; ?>" class="title"><?php echo htmlentities($dnn2['title'], ENT_QUOTES, 'UTF-8'); ?></a>
			    </li>
			<?php
			} ?>
				<li>
					<?php echo pagination($statement,$limit,$page, $reload="?");?>
				</li>
			</ul>
		</div>
		<div class="container content birthday">
			<div class="pad-10 fb-page"
		  data-href="https://web.facebook.com/NovelleCenter/"
		  data-width="380"
		  data-small-header="true"
		  data-hide-cover="false"
		  data-show-facepile="true"
		  data-show-posts="false"></div>

			<div>
					<?php
						$dob = [];
						$dob_count = 0;
						$user = mysql_query("SELECT id, username, dob, gender FROM users");
						if(mysql_num_rows($user)>0){
							while($u_dob = mysql_fetch_array($user)){
								$dob[$dob_count] = date('j, n', $u_dob['dob']);
								$dob_count++;
							}
							if (in_array(date('j, n', time()), $dob)) { ?>
								<h3>Birthday(s):</h3>
									<ul class="pad-10">
								<?php
								$user = mysql_query("SELECT id, username, dob, gender FROM users");
								while($u = mysql_fetch_array($user)){
									if(date('j n', $u['dob']) == date('j n', time())) { ?>

									<li><a href="profile.php?id=<?php echo $u['id']; ?>"> <?php echo $u['username'];
									if ($u['gender'] == "m") {?>
										(<span style="color: #44b;">m</span>)</a>
									<?php
									}else{ ?>
										(<span style="color:#b4b;">f</span>)</a>
									<?php
									}
									?>
									</li>
									<?php
									}
								}
								?>
								</ul>
							<?php
							}
						} ?>
			</div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>