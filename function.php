<?php
function check_time($date_value){
	$interv = mktime() - date($date_value);
    if ($interv < 86400) {
        return date("@g:i a", $date_value);
    }else{
    	return date("@F j, Y, g:i a", $date_value);
    }
}

function pagination($query, $per_page = 10,$page = 1, $reload = '', $message_c = 1, $message_s = 0){
	$query = "SELECT COUNT(*) as 'num' FROM {$query}";
	$row = mysql_fetch_array(mysql_query($query));
	if(!$row) {
		$message = 'Oooops! Out of Data';
	}
	if ($message_s == 1){
		$total = $message_c;
	}else{
		$total = $row['num'];
	}

	$tpages = ceil($total / $per_page);
    $adjacents = 2;

	$prevlabel = "&lsaquo; Prev";
	$nextlabel = "Next &rsaquo;";


	$out = "<ul class=\"pagination\">\n";

	// previous
	if($page==0) {
		// $out.= "<li>" . $prevlabel . "</li>\n";
		// header('Location: 404.php\#');
		// exit;
	}
	elseif($page==1) {
		// header("location: ../index.php");

	}
	elseif($page==2) {
		$out.= "<a href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
	}
	elseif($page > $tpages) {
		$out.= "<a href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
	}
	else {
		$out.= "<a href=\"" . $reload . "page=" . ($page-1) . "\">" . $prevlabel . "</a>\n";
	}

	// first
	if($page>($adjacents+1)) {
		$out.= "<a href=\"" . $reload . "\">1</a>\n";
	}

	// interval
	if($page>($adjacents+2)) {
		$out.= "...\n";
	}

	// pages
	$pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
	$pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
	for($i=$pmin; $i<=$pmax; $i++) {
		if($i==$page) {
			$out.= "<li class=\"current\">" . $i . "</li>\n";
		}
		elseif($i==1) {
			$out.= "<a href=\"" . $reload . "\">" . $i . "</a>\n";
		}
		else {
			$out.= "<a href=\"" . $reload . "page=" . $i . "\">" . $i . "</a>\n";
		}
	}

	// interval
	if($page<($tpages-$adjacents-1)) {
		$out.= "...\n";
	}

	// last
	if($page<($tpages-$adjacents)) {
		$out.= "<a href=\"" . $reload . "page=" . $tpages . "\">" . $tpages . "</a>\n";
	}

	// next
	if($page<$tpages) {
		$out.= "<a href=\"" . $reload . "page=" . ($page+1) . "\">" . $nextlabel . "</a>\n";
	}
	// else {
		// $out.= "<li>" . $nextlabel . "</li>\n";
	// }

	$out.= "<li class=\"current\">(Page " . $page . " of " . $tpages . ")</li>\n</ul>";

	return $out;
}

function r_pagination($query, $per_page = 10,$page = 1, $reload = '', $message_c = 1, $message_s = 0){
	$query = "SELECT COUNT(*) as 'num' FROM {$query}";
	$row = mysql_fetch_array(mysql_query($query));
	if ($message_s == 1){
		$total = $message_c;
	}else{
		$total = $row['num'];
	}

	$tpages = ceil($total / $per_page);
    $adjacents = 2;

	$prevlabel = "&lsaquo; Prev";
	$nextlabel = "Next &rsaquo;";


	$out = "<ul class=\"pagination\">\n";

	// previous
	if($page==0) {
		// $out.= "<li>" . $prevlabel . "</li>\n";
		// header('Location: 404.php\#');
		// exit;
	}
	elseif($page==1) {
		// header("location: ../index.php");

	}
	elseif($page==2) {
		$out.= "<a href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
	}
	elseif($page > $tpages) {
		$out.= "<a href=\"" . $reload . "\">" . $prevlabel . "</a>\n";
	}
	else {
		$out.= "<a href=\"" . $reload . "rpage=" . ($page-1) . "\">" . $prevlabel . "</a>\n";
	}

	// first
	if($page>($adjacents+1)) {
		$out.= "<a href=\"" . $reload . "\">1</a>\n";
	}

	// interval
	if($page>($adjacents+2)) {
		$out.= "...\n";
	}

	// pages
	$pmin = ($page>$adjacents) ? ($page-$adjacents) : 1;
	$pmax = ($page<($tpages-$adjacents)) ? ($page+$adjacents) : $tpages;
	for($i=$pmin; $i<=$pmax; $i++) {
		if($i==$page) {
			$out.= "<li class=\"current\">" . $i . "</li>\n";
		}
		elseif($i==1) {
			$out.= "<a href=\"" . $reload . "\">" . $i . "</a>\n";
		}
		else {
			$out.= "<a href=\"" . $reload . "rpage=" . $i . "\">" . $i . "</a>\n";
		}
	}

	// interval
	if($page<($tpages-$adjacents-1)) {
		$out.= "...\n";
	}

	// last
	if($page<($tpages-$adjacents)) {
		$out.= "<a href=\"" . $reload . "rpage=" . $tpages . "\">" . $tpages . "</a>\n";
	}

	// next
	if($page<$tpages) {
		$out.= "<a href=\"" . $reload . "rpage=" . ($page+1) . "\">" . $nextlabel . "</a>\n";
	}
	// else {
		// $out.= "<li>" . $nextlabel . "</li>\n";
	// }

	$out.= "<li class=\"current\">(Page " . $page . " of " . $tpages . ")</li>\n</ul>";

	return $out;
}
?>