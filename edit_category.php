<?php
//This page let an administrator edit a category
include('config.php');
if(isset($_GET['id']))
{
$id = intval($_GET['id']);
$dn1 = mysql_fetch_array(mysql_query('select count(id) as nb1, name, description, sub_id from categories where id="'.$id.'" group by id'));
if($dn1['nb1']>0)
{
if(isset($_SESSION['username']) and ($_SESSION['username']==$admin || $_SESSION['perm'] >= 1))
{
?>
<!DOCTYPE html>
<html >
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Edit a category - <?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?> - Forum</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
    	<div class="container">
    		<?php include ('ads.php'); ?>
    	</div>
        <div class="container content">
<div class="page-title page-breadcrumb">
	<ul>
		<li>
			<a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
		</li>
		<li>
			<?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?>
		</li>
		<li>
			Edit the category
		</li>
	</ul>
</div>
<div class="main-content">
<?php
if(isset($_POST['name'], $_POST['description']) and $_POST['name']!='')
{
	$name = $_POST['name'];
	$description = $_POST['description'];
	if(get_magic_quotes_gpc())
	{
		$name = stripslashes($name);
		$description = stripslashes($description);
	}
	$name = mysql_real_escape_string($name);
	$description = mysql_real_escape_string($description);
	if(mysql_query('update categories set name="'.$name.'", description="'.$description.'", sub_id="'.$sub_id.'" where id="'.$id.'"'))
	{
	?>
	<div class="message">The category have successfully been edited...</div>
	<?php
	header("refresh:3; index.php");
	}
	else
	{
		echo 'An error occured while editing the category.';
	}
}
else
{
?>
	<form action="edit_category.php?id=<?php echo $id; ?>" method="post">
		<label for="name">Name</label><input type="text" name="name" id="name" value="<?php echo htmlentities($dn1['name'], ENT_QUOTES, 'UTF-8'); ?>" /><br />
		<label for="sub_id" >Sub Category</label>
		<select name="sub_id" id="sub_id">
			<option <?php if($dn1['sub_id'] == '1'){echo "selected";} ?>>1</option>
			<option <?php if($dn1['sub_id'] == '2'){echo "selected";} ?>>2</option>
			<option <?php if($dn1['sub_id'] == '3'){echo "selected";} ?>>3</option>
		</select><br />
		<label for="description">Description</label>(html enabled)<br />
	    <textarea name="description" id="description" cols="70" rows="6"><?php echo htmlentities($dn1['description'], ENT_QUOTES, 'UTF-8'); ?></textarea><br />
	    <input type="submit" value="Edit" />
	</form>
<?php
}
?>
	</div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>
<?php
}
else
{
	echo '<h2>You must be logged as an administrator to access this page: <a href="login.php">Login</a> - <a href="signup.php">Sign Up</a></h2>';
}
}
else
{
	echo '<h2>The category you want to edit doesn\'t exist..</h2>';
}
}
else
{
	echo '<h2>The ID of the category you want to edit is not defined.</h2>';
}
?>