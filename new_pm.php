<?php
//This page let create a new personnal message
include('config.php');
include('user-right.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>New PM</title>
    </head>
    <body>
    	<?php include_once ('header.php'); ?>
    	<div class="container">
            <?php include ('ads.php'); ?>
        </div>
<?php
if(isset($_SESSION['username'])){
$form = true;
$otitle = '';
$orecip = '';
$omessage = '';

if(isset($_POST['title'], $_POST['recip'], $_POST['message'])){
	$otitle = $_POST['title'];
	$orecip = $_POST['recip'];
	$omessage = $_POST['message'];

	if(get_magic_quotes_gpc()){
		$otitle = stripslashes($otitle);
		$orecip = stripslashes($orecip);
		$omessage = stripslashes($omessage);
	}

	if($_POST['title']!='' and $_POST['recip']!='' and $_POST['message']!=''){
		$title = mysql_real_escape_string($otitle);
		$recip = mysql_real_escape_string($orecip);
		$message = mysql_real_escape_string(nl2br(htmlentities($omessage, ENT_QUOTES, 'UTF-8')));

		$recip = preg_split("/[,]+/", $recip);
		$recip_count = count($recip);

		for($i = 0; $i < $recip_count; ++$i){
			$dn1[$i] = mysql_fetch_array(mysql_query('select count(id) as recip, id as recipid, (select count(*) from pm) as npm from users where username="'. trim($recip[$i]).'"'));

			if($dn1[$i]['recip'] == 1){
				if($dn1[$i]['recipid']!=$_SESSION['userid']){
					$form = false;
				}else{
					$error = 'You cannot send a PM to yourself.';
				}
			}else{
				$error = 'The'. $i .'recipient of your PM doesn\'t exist.';
			}
		}

		if (!$form) {
			for ($i=1; $i < $recip_count+1; $i++) {
				// process message
				$dn1[$i] = mysql_fetch_array(mysql_query('select count(id) as recip, id as recipid, (select count(*) from pm) as npm from users where username="'. trim($recip[$i-1]).'"'));

				$id = $dn1[$i]['npm']+ 1;

				$pm_query = mysql_query('insert into pm (id, id2, title, user1, user2, message, timestamp, user1read, user2read)values("'.$id.'", "1", "'.$title.'", "'.$_SESSION['userid'].'", "'.$dn1[$i]['recipid'].'", "'.$message.'", "'.time().'", "yes", "no")');
				if($pm_query) {
					$error = 'The PM have successfully been sent.';

					if(isset($_FILES['image']['name']) and !empty($_FILES['image']['name'])) {
			      $status= array();
			      $file_name = $_FILES['image']['name'];
			      $file_size = $_FILES['image']['size'];
			      $file_tmp = $_FILES['image']['tmp_name'];
			      $file_type = $_FILES['image']['type'];
			      $file_string = explode('.',$file_name);
			      $file_ext = strtolower(end($file_string));
			      $file_new_name = md5(uniqid(rand(), true)) .'.'. $file_ext;

			      $expensions= array("jpeg","jpg","png","doc","docx","xlsx","ppt", "pptx", "pdf");

			      if(in_array($file_ext,$expensions)=== false){
			        $status="Extension not allowed, please choose a JPEG or PNG file.";

				      	if($file_size > 2097152) {
				         $status='File size must be excately 2 MB';
				      }
			      }

			      if(empty($status)==true) {
			      	move_uploaded_file($file_tmp,"uploads/".$file_new_name);
			      	$pm_upload = mysql_query('insert into pm_uploads (id1, id2, real_name, new_name)values("'.$id.'", "1", "'.$file_name.'", "'.$file_new_name.'")');
			      	// die()
			      	$status="Success";
			      }
			   }


					$form = false;
				}else{
					$error = 'An error occurred while sending the PM.';
				}
			}
		}

	}else{
		$error = 'A field is not filled.';
	}
}
elseif(isset($_GET['recip']))
{
	$orecip = $_GET['recip'];
}

if(isset($error)){?>
	<div class="container content">
		<div class="main-content">
			<div class="message">
				<?php echo $error; ?>
				<?php header("refresh:3; list_pm.php"); ?>
			</div>
		</div>
	</div>
<?php
}

if($form){
?>
<div class="container content">

<div class="page-title page-breadcrumb">
    <ul>
        <li>
            <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
        </li>
        <li>
            <a href="list_pm.php">Messages</a>
        </li>
        <li>
            New PM
        </li>
    </ul>
</div>
<div class="main-content">
	<h1>New Personal Message</h1>
    <form action="new_pm.php" method="post" enctype="multipart/form-data">
		Please fill this form to send a PM:<br />
        <label for="title">Title</label><input type="text" value="<?php echo htmlentities($otitle, ENT_QUOTES, 'UTF-8'); ?>" id="title" name="title" /><br />
        <label for="recip">Recipient<span class="small">(Username)</span></label><input type="text" value="<?php echo htmlentities($orecip, ENT_QUOTES, 'UTF-8'); ?>" id="recip" name="recip" /><br />
        <label for="message">Message</label><textarea cols="40" rows="5" id="message" name="message"><?php echo htmlentities($omessage, ENT_QUOTES, 'UTF-8'); ?></textarea><br />
        <label> Attachment (jpeg, jpg, png, doc, docx, xlsx, ppt, pptx, pdf)
		    <input type="file" name = "image" /></label><br /><br />
		    <input type="submit" value="Send" />
    </form>
</div>
<?php
}
}
else
{
?>
<div class="message">You must be logged to access this page.</div>
<div class="box_login">
	<form action="login.php" method="post">
		<label for="username">Username</label><input type="text" name="username" id="username" /><br />
		<label for="password">Password</label><input type="password" name="password" id="password" /><br />
        <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" />
        <div class="center">
	        <input type="submit" value="Login" /> <input type="button" onclick="javascript:document.location='signup.php';" value="Sign Up" />
        </div>
    </form>

</div>
<?php
}
?>
	</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>