<?php
//This page display the profile of an user
include('config.php');
include('user-right.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Profile of an user</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
        <div class="container">
            <?php include ('ads.php'); ?>
        </div>
        <div class="container content">

        <div class="page-title page-breadcrumb">
            <ul>
                <li>
                    <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
                </li>
                <li>
                    Profile
                </li>
            </ul>
        </div>
        <div class="main-content">
<?php
if(isset($_GET['id']))
{
	$id = intval($_GET['id']);
	$dn = mysql_query('select id, username, email, dob, avatar, signup_date, ban, perm_level from users where id="'.$id.'"');
	if(mysql_num_rows($dn)>0)
	{
		$dnn = mysql_fetch_array($dn);
?>
This is the profile of "<?php echo htmlentities($dnn['username']); ?>" :

<br />
<div class="">
    <?php
    if($_SESSION['userid']==$id){ ?>

    <a href="edit_profile.php" class="button">Edit my profile</a>
    <?php
    }
    if($_SESSION['username']==$admin || $_SESSION['perm'] >= 1){
        if ($_SESSION['userid'] != htmlentities($dnn['id'])) {
            if(htmlentities($dnn['ban'] == 0)){ ?>
                <a href="ban.php?status=1&id=<?php echo $id?>" class="button">Ban User</a>
            <?php
            }else{ ?>
                <a href="ban.php?status=0&id=<?php echo $id?>" class="button">Unban User</a>
            <?php
            }

            if(htmlentities($dnn['perm_level'] == 0)){ ?>
                <a href="make_admin.php?status=1&id=<?php echo $id?>" class="button">Make Admin</a>
            <?php
            }else{ ?>
                <a href="make_admin.php?status=0&id=<?php echo $id?>" class="button">Remove Admin</a>
            <?php
            }
        }
    }?>
</div>
<table style="text-align: left;">
	<tr>
    	<td><?php
if($dnn['avatar']!='')
{
	echo '<img src="'.htmlentities($dnn['avatar'], ENT_QUOTES, 'UTF-8').'" alt="Avatar" style="max-width:100px;max-height:100px;" />';
}
else
{
	echo 'This user has no avatar.';
}
?></td>
    	<td class="left"><h1><?php echo htmlentities($dnn['username'], ENT_QUOTES, 'UTF-8'); ?></h1>
        Email: <?php echo htmlentities($dnn['email'], ENT_QUOTES, 'UTF-8'); ?><br />
    	Dob: <?php echo date('j F, Y',$dnn['dob']); ?><br />
        This user joined the website on <?php echo date('Y/m/d',$dnn['signup_date']); ?></td>
    </tr>
</table>
<?php
if(isset($_SESSION['username']) and $_SESSION['username']!=$dnn['username'])
{
?>
<br /><a href="new_pm.php?recip=<?php echo urlencode($dnn['username']); ?>" class="big">Send "<?php echo htmlentities($dnn['username'], ENT_QUOTES, 'UTF-8'); ?>" a private message</a>
<?php
}
	}
	else
	{
		echo 'This user doesn\'t exist.';
	}
}
else
{
	echo 'The ID of this user is not defined.';
}
?>	</div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>