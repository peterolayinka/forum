<?php
//This page let users sign up
include('config.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Sign Up</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
    	 <div class="container">
            <?php include ('ads.php'); ?>
        </div>
<?php
include('admin-right.php');

if(isset($_POST['username'], $_POST['password'], $_POST['passverif'], $_POST['email'], $_POST['gender'], $_POST['avatar']) and $_POST['username']!=''){
	if(get_magic_quotes_gpc()){
		$_POST['username'] = stripslashes($_POST['username']);
		$_POST['password'] = stripslashes($_POST['password']);
		$_POST['passverif'] = stripslashes($_POST['passverif']);
		$_POST['email'] = stripslashes($_POST['email']);
		$_POST['gender'] = stripslashes($_POST['gender']);
		$_POST['avatar'] = stripslashes($_POST['avatar']);
	}
	if($_POST['password']==$_POST['passverif']){
		if(strlen($_POST['password'])>=6){
			if(preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i',$_POST['email'])){
				$username = mysql_real_escape_string($_POST['username']);
				$password = mysql_real_escape_string(sha1($_POST['password']));
				$email = mysql_real_escape_string($_POST['email']);
				$gender = mysql_real_escape_string($_POST['gender']);
				$dob = strtotime(str_replace('/', '-', $_POST['dob']));
				$avatar = mysql_real_escape_string($_POST['avatar']);
				$dn = mysql_num_rows(mysql_query('select id from users where username="'.$username.'"'));
				if($dn==0){
					$dn2 = mysql_num_rows(mysql_query('select id from users'));
					$id = $dn2+1;
					if(mysql_query('insert into users(id, username, password, email, dob, gender, avatar, signup_date) values ('.$id.', "'.$username.'", "'.$password.'", "'.$email.'", "'.$dob.'", "'.$gender.'", "'.$avatar.'", "'.time().'")')){
						$form = false;
?>
<div class="container content">
	<div class="main-content">
		<div class="message">You have successfully been signed up a new user<br />
			<a href="signup.php">Add new user</a>
		</div>
	</div>
</div>
<?php
					}else{
						$form = true;
						$message = 'An error occurred while signing you up.';
					}
				}else{
					$form = true;
					$message = 'Another user already use this username.';
				}
			}else{
				$form = true;
				$message = 'The email you typed is not valid.';
			}
		}else{
			$form = true;
			$message = 'Your password must have a minimum of 6 characters.';
		}
	}else{
		$form = true;
		$message = 'The passwords you entered are not identical.';
	}
}else{
	$form = true;
}
if($form){
	if(isset($message)){ ?>
		<div class="container content">
			<div class="main-content">
				<div class="message"> <?php echo $message ?> </div>
			</div>
		</div>
	<?php
	}
?>
<div class="container content">
	<div class="page-title page-breadcrumb">
	    <ul>
	        <li>
	            <a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
	        </li>
	        <li>
	            Add User
	        </li>
	    </ul>
	</div>
	<div class="main-content">
	    <form action="signup.php" method="post">
	        Please fill this form to sign up:<br />
	        <div class="">
	            <label for="username">Username</label><input id="username" type="text" name="username" value="<?php if(isset($_POST['username'])){echo htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8');} ?>" /><br />
	            <label for="password">Password<span class="small">(6 characters min.)</span></label><input id="password" type="password" name="password" /><br />
	            <label for="passverif">Password<span class="small">(verification)</span></label><input id="passverif" type="password" name="passverif" /><br />
	            <label for="email">Email</label><input id="email" type="text" name="email" value="<?php if(isset($_POST['email'])){echo htmlentities($_POST['email'], ENT_QUOTES, 'UTF-8');} ?>" /><br />
	            <label for="gender">Gender</label>
	            <select id="gender" name="gender">
	            	<option value="m">male</option>
	            	<option value="f">female</option>
	            </select>
	            <br />
	            <label for="dob">Date of Birth</label><input placeholder="dd/mm/yy" type="text" id="dob" name="dob" value="<?php if(isset($_POST['dob'])){echo htmlentities($_POST['dob'], ENT_QUOTES, 'UTF-8');} ?>" /><br />
	            <label for="avatar">Avatar<span class="small">(optional)</span></label><input id="avatar" type="text" name="avatar" value="<?php if(isset($_POST['avatar'])){echo htmlentities($_POST['avatar'], ENT_QUOTES, 'UTF-8');} ?>" /><br />
	            <input type="submit" value="Sign Up" />
			</div>
	    </form>
	</div>
<?php
}
?>
	</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>