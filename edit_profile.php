<?php
//This page let an user edit his profile
include('config.php');
include('user-right.php');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="<?php echo $design; ?>/style.css" rel="stylesheet" title="Style" />
        <title>Edit your profile</title>
    </head>
    <body>
    	<?php include_once ('header.php');?>
    	<div class="container">
    		<?php include ('ads.php'); ?>
    	</div>
		<div class="container content">
		<?php
if(isset($_SESSION['username'])){
?>
<div class="page-title page-breadcrumb">
	<ul>
		<li>
			<a href="<?php echo $url_home; ?>">Novelle Center Forum</a>
		</li>
		<li>
			Edit profile
		</li>
	</ul>
</div>
<div class="main-content">
<?php
	if(isset($_POST['password'], $_POST['passverif'], $_POST['email'], $_POST['avatar'])){
		if(get_magic_quotes_gpc()){
			$_POST['username'] = stripslashes($_POST['username']);
			$_POST['password'] = stripslashes($_POST['password']);
			$_POST['passverif'] = stripslashes($_POST['passverif']);
			$_POST['email'] = stripslashes($_POST['email']);
			$_POST['gender'] = stripslashes($_POST['gender']);
			$_POST['avatar'] = stripslashes($_POST['avatar']);
		}
		if($_POST['password']==$_POST['passverif']){
			if(strlen($_POST['password'])>=6){
				if(preg_match('#^(([a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+\.?)*[a-z0-9!\#$%&\\\'*+/=?^_`{|}~-]+)@(([a-z0-9-_]+\.?)*[a-z0-9-_]+)\.[a-z]{2,}$#i',$_POST['email'])){

					$username = mysql_real_escape_string($_POST['username']);
					$password = mysql_real_escape_string(sha1($_POST['password']));
					$email = mysql_real_escape_string($_POST['email']);
					$gender = mysql_real_escape_string($_POST['gender']);
					$dob = strtotime(str_replace('/', '-', $_POST['dob']));
					$avatar = mysql_real_escape_string($_POST['avatar']);
					$dn = mysql_fetch_array(mysql_query('select count(*) as nb from users where username="'.$username.'"'));

					if($dn['nb']==0 or $_POST['username']==$_SESSION['username']){
						if(mysql_query('update users set  password="'.$password.'", email="'.$email.'", dob="'.$dob.'", gender="'.$gender.'", avatar="'.$avatar.'" where id="'.mysql_real_escape_string($_SESSION['userid']).'"')){

								$form = false;
								unset($_SESSION['username'], $_SESSION['userid']);
								?>
								<div class="message">Your profile have successfully been edited. You must login again.</div>
								<?php
								header("refresh:3; url=index.php");
						}else{
							$form = true;
							$message = 'An error occured while editing your profile.';
						}
					}else{
						$form = true;
						$message = 'Another user already uses this username.';
					}
				}else{
					$form = true;
					$message = 'The email you typed is not valid.';
				}
			}else{
				$form = true;
				$message = 'Your password must have a minimum of 6 characters.';
			}
		}else{
			$form = true;
			$message = 'The passwords you entered are not identical.';
		}
	}else{
		$form = true;
		$message = 'Details aren\'t set accurately';
	}
	if($form)
	{
		if(isset($message))
		{
			echo '<strong>'.$message.'</strong>';
		}
		if(isset($_POST['username'],$_POST['password'],$_POST['email']))
		{
			$username = htmlentities($_POST['username'], ENT_QUOTES, 'UTF-8');
			if($_POST['password']==$_POST['passverif'])
			{
				$password = htmlentities($_POST['password'], ENT_QUOTES, 'UTF-8');
			}
			else
			{
				$password = '';
			}
			$email = htmlentities($_POST['email'], ENT_QUOTES, 'UTF-8');
			$avatar = htmlentities($_POST['avatar'], ENT_QUOTES, 'UTF-8');
		}
		else
		{
			$dnn = mysql_fetch_array(mysql_query('select username,email, dob, gender,avatar from users where username="'.$_SESSION['username'].'"'));
			$username = htmlentities($dnn['username'], ENT_QUOTES, 'UTF-8');
			$password = '';
			$email = htmlentities($dnn['email'], ENT_QUOTES, 'UTF-8');
			$dob = htmlentities($dnn['dob'], ENT_QUOTES, 'UTF-8');
			$gender = htmlentities($dnn['gender'], ENT_QUOTES, 'UTF-8');
			$avatar = htmlentities($dnn['avatar'], ENT_QUOTES, 'UTF-8');
		}
?>
    <form action="edit_profile.php" method="post">
        You can edit your informations:<br />
        <div class="">
            <label for="username">Username</label><input disabled type="text" name="username" id="username" value="<?php echo $username; ?>" /><br />
            <label for="password">Password<span class="small">(6 characters min.)</span></label><input type="password" name="password" id="password" value="<?php echo $password; ?>" /><br />
            <label for="passverif">Password<span class="small">(verification)</span></label><input type="password" name="passverif" id="passverif" value="<?php echo $password; ?>" /><br />
            <label for="email">Email</label><input type="text" name="email" id="email" value="<?php echo $email; ?>" /><br />
            <label for="gender">Gender</label>
	            <select id="gender" name="gender">
	            	<option value="m" <?php if ($gender == 'm') {echo 'selected'; } ?>>male</option>
	            	<option value="f" <?php if ($gender == 'f') {echo 'selected'; } ?>>female</option>
	            </select>
	            <br />
	            <label for="dob">Date of Birth</label><input placeholder="dd/mm/yy" type="text" id="dob" name="dob" value="<?php echo date('d/m/Y', $dob); ?>" /><br />
            <label for="avatar">Avatar<span class="small">(optional)</span></label><input type="text" name="avatar" id="avatar" value="<?php echo $avatar; ?>" /><br />
            <input type="submit" value="Submit" />
        </div>
    </form>

<?php
	}
}else{
?>
<h2>You must be logged to access this page:</h2>
<div class="box_login">
	<form action="login.php" method="post">
		<label for="username">Username</label><input type="text" name="username" id="username" /><br />
		<label for="password">Password</label><input type="password" name="password" id="password" /><br />
        <label for="memorize">Remember</label><input type="checkbox" name="memorize" id="memorize" value="yes" />
        <div class="center">
	        <input type="submit" value="Login" /> <input type="button" onclick="javascript:document.location='signup.php';" value="Sign Up" />
        </div>
    </form>
</div>
<?php
}
?>
	</div>
		</div>
		<?php include_once ('footer.php'); ?>
	</body>
</html>